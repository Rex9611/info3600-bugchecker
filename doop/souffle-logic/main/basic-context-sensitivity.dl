.comp BasicContextSensitivity<Configuration> : AbstractContextSensitivity<Configuration> {

// The main analysis is merely looking up the previously created
// context objects.

#ifdef ZIPPER

.decl ZipperPrecisionCriticalMethod(?method:Method)
.input ZipperPrecisionCriticalMethod(IO="file",filename="ZipperPrecisionCriticalMethod.facts",delimiter="\t")

// virtual invocation
isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, ?hctx, ?invocation, ?value), 
  configuration.ContextResponse(?callerCtx, ?hctx, ?invocation, ?value, ?calleeCtx),
  Value_Type(?value, ?valuetype),
  basic.ResolveInvocation(?valuetype, ?invocation, ?tomethod),
  ZipperPrecisionCriticalMethod(?tomethod).
#ifndef X_CONTEXT_REMOVER
// .plan 1:(2,1,3,4), 2:(3,1,2,4)
#endif

CallGraphEdge(?callerCtx, ?invocation, ?immCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, _, ?invocation, ?value), 
  isImmutableContext(?immCtx),
  Value_Type(?value, ?valuetype),
  basic.ResolveInvocation(?valuetype, ?invocation, ?tomethod),
  !ZipperPrecisionCriticalMethod(?tomethod).
#ifndef X_CONTEXT_REMOVER
// .plan 1:(2,1,3,4), 2:(3,1,2,4)
#endif

// special invocation
isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, ?hctx, ?invocation, ?value),
  configuration.ContextResponse(?callerCtx, ?hctx, ?invocation, ?value, ?calleeCtx),
  MethodInvocation_Method(?invocation, ?tomethod),
  isSpecialMethodInvocation_Insn(?invocation),
  ZipperPrecisionCriticalMethod(?tomethod).
#ifndef X_CONTEXT_REMOVER
// .plan 1:(2,1,3,4)
#endif

CallGraphEdge(?callerCtx, ?invocation, ?immCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, _, ?invocation, _),
  isImmutableContext(?immCtx),
  MethodInvocation_Method(?invocation, ?tomethod),
  isSpecialMethodInvocation_Insn(?invocation),
  !ZipperPrecisionCriticalMethod(?tomethod).
#ifndef X_CONTEXT_REMOVER
// .plan 1:(2,1,3,4)
#endif

// static invocation

configuration.StaticContextRequest(?callerCtx, ?invocation) :-
  ReachableContext(?callerCtx, ?inmethod),
  StaticMethodInvocation(?invocation, _, ?inmethod).

isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.StaticContextRequest(?callerCtx, ?invocation),
  configuration.StaticContextResponse(?callerCtx, ?invocation, ?calleeCtx),
  MethodInvocation_Method(?invocation, ?tomethod),
  ZipperPrecisionCriticalMethod(?tomethod).

CallGraphEdge(?callerCtx, ?invocation, ?immCtx, ?tomethod) :-
  configuration.StaticContextRequest(?callerCtx, ?invocation),
  isImmutableContext(?immCtx),
  MethodInvocation_Method(?invocation, ?tomethod),
  !ZipperPrecisionCriticalMethod(?tomethod).

#else
// Original rules

isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, ?hctx, ?invocation, ?value), 
  configuration.ContextResponse(?callerCtx, ?hctx, ?invocation, ?value, ?calleeCtx),
  Value_Type(?value, ?valuetype),
  basic.ResolveInvocation(?valuetype, ?invocation, ?tomethod).
#ifndef X_CONTEXT_REMOVER
 .plan 1:(2,1,3,4), 2:(3,1,2,4)
#endif
 
isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.ContextRequest(?callerCtx, ?hctx, ?invocation, ?value),
  configuration.ContextResponse(?callerCtx, ?hctx, ?invocation, ?value, ?calleeCtx),
  MethodInvocation_Method(?invocation, ?tomethod),
  isSpecialMethodInvocation_Insn(?invocation).
#ifndef X_CONTEXT_REMOVER
 .plan 1:(2,1,3,4)
#endif

configuration.StaticContextRequest(?callerCtx, ?invocation) :-
  ReachableContext(?callerCtx, ?inmethod),
  StaticMethodInvocation(?invocation, _, ?inmethod).

isContext(?calleeCtx),
CallGraphEdge(?callerCtx, ?invocation, ?calleeCtx, ?tomethod) :-
  configuration.StaticContextRequest(?callerCtx, ?invocation),
  configuration.StaticContextResponse(?callerCtx, ?invocation, ?calleeCtx),
  MethodInvocation_Method(?invocation, ?tomethod).

#endif // #ifdef ZIPPER

isContext(?newCtx),
CallGraphEdge(?callerCtx, ?fakeinvo, ?newCtx, ?register) :-
  FakeInvocation_RegisterFinalize(?heapValue, ?fakeinvo),
  basic.AllocatedObjectSupportsFinalize(?heapValue, ?inmethod),
  (!(HeapAllocation_Merge(?heapValue, _));
   HeapAllocation_Merge(?heapValue, ?heapValue)),  // the object retains its identity
  ReachableContext(?callerCtx, ?inmethod),
  ?register = "<java.lang.ref.Finalizer: void register(java.lang.Object)>",
  isMethod(?register),
  FormalParam(0, ?register, ?formal),
  configuration.FinalizerRegisterContextRequest(?callerCtx, ?inmethod, ?heapValue),  
  configuration.FinalizerRegisterContextResponse(?callerCtx, ?inmethod, ?heapValue, ?newCtx).

}
