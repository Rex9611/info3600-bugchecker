package org.clyze.doop.soot;

import soot.*;
import soot.jimple.*;
import soot.shimple.Shimple;
import org.clyze.doop.common.Database;
import org.clyze.doop.common.PredicateFile;
import java.io.*;


import static org.clyze.doop.common.PredicateFile.*;

import java.lang.*;

public class DiggerLineNumber {
    static private File outDir;

    static protected void writeLineNumber(FactWriter _writer, SootMethod m, Body b, Session session) {
            for(Unit u : b.getUnits()) {
                if (u instanceof Stmt) {
                    try {
                        _writer.writeInstructionLine(m, u, session);
                    } catch (RuntimeException e) {
                        System.err.println(e.toString());
                        continue;
                    }
                }
            }
    }

    static void setOutDir(File dir) {
        if(outDir == null){
            outDir = new File(dir, "Instruction-Line.facts");
        }
    }
}
