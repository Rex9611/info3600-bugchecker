// Generated from org/clyze/jimple/Jimple.g4 by ANTLR 4.5.1

package org.clyze.jimple;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JimpleParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		INTEGER=67, MARKER=68, REAL=69, BOOLEAN=70, STRING=71, IDENTIFIER=72, 
		OP=73, WHITE_SPACE=74;
	public static final int
		RULE_program = 0, RULE_klass = 1, RULE_modifier = 2, RULE_field = 3, RULE_method = 4, 
		RULE_throwsExceptions = 5, RULE_identifierList = 6, RULE_methodBody = 7, 
		RULE_statement = 8, RULE_declarationStmt = 9, RULE_complexAssignmentStmt = 10, 
		RULE_assignmentStmt = 11, RULE_returnStmt = 12, RULE_invokeStmt = 13, 
		RULE_allocationStmt = 14, RULE_methodSig = 15, RULE_dynamicMethodSig = 16, 
		RULE_fieldSig = 17, RULE_value = 18, RULE_valueList = 19, RULE_bootValueList = 20, 
		RULE_jumpStmt = 21, RULE_switchStmt = 22, RULE_caseStmt = 23, RULE_catchStmt = 24, 
		RULE_monitorStmt = 25, RULE_nopStmt = 26;
	public static final String[] ruleNames = {
		"program", "klass", "modifier", "field", "method", "throwsExceptions", 
		"identifierList", "methodBody", "statement", "declarationStmt", "complexAssignmentStmt", 
		"assignmentStmt", "returnStmt", "invokeStmt", "allocationStmt", "methodSig", 
		"dynamicMethodSig", "fieldSig", "value", "valueList", "bootValueList", 
		"jumpStmt", "switchStmt", "caseStmt", "catchStmt", "monitorStmt", "nopStmt"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'class'", "'interface'", "'extends'", "'implements'", "'{'", "'}'", 
		"'public'", "'protected'", "'private'", "'static'", "'abstract'", "'final'", 
		"'transient'", "'synchronized'", "'volatile'", "'native'", "'enum'", "'strictfp'", 
		"'[]'", "';'", "'('", "')'", "'throws'", "','", "':'", "'['", "']'", "'='", 
		"'.'", "':='", "'@caughtexception'", "'lengthof'", "'neg'", "'cmp'", "'cmpl'", 
		"'cmpg'", "'instanceof'", "'Phi'", "'return'", "'specialinvoke'", "'virtualinvoke'", 
		"'interfaceinvoke'", "'staticinvoke'", "'dynamicinvoke'", "'new'", "'newarray'", 
		"'newmultiarray'", "'<'", "'>'", "'handle:'", "'=='", "'!='", "'<='", 
		"'>='", "'goto'", "'tableswitch'", "'lookupswitch'", "'case'", "'default'", 
		"'catch'", "'from'", "'to'", "'with'", "'entermonitor'", "'exitmonitor'", 
		"'nop'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, "INTEGER", "MARKER", "REAL", 
		"BOOLEAN", "STRING", "IDENTIFIER", "OP", "WHITE_SPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Jimple.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JimpleParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public KlassContext klass() {
			return getRuleContext(KlassContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			klass();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KlassContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public List<FieldContext> field() {
			return getRuleContexts(FieldContext.class);
		}
		public FieldContext field(int i) {
			return getRuleContext(FieldContext.class,i);
		}
		public List<MethodContext> method() {
			return getRuleContexts(MethodContext.class);
		}
		public MethodContext method(int i) {
			return getRuleContext(MethodContext.class,i);
		}
		public KlassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_klass; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterKlass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitKlass(this);
		}
	}

	public final KlassContext klass() throws RecognitionException {
		KlassContext _localctx = new KlassContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_klass);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17))) != 0) || _la==IDENTIFIER) {
				{
				{
				setState(56);
				modifier();
				}
				}
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(62);
			_la = _input.LA(1);
			if ( !(_la==T__0 || _la==T__1) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(63);
			match(IDENTIFIER);
			setState(66);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(64);
				match(T__2);
				setState(65);
				match(IDENTIFIER);
				}
			}

			setState(70);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(68);
				match(T__3);
				setState(69);
				identifierList(0);
				}
			}

			setState(72);
			match(T__4);
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17))) != 0) || _la==IDENTIFIER) {
				{
				setState(75);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(73);
					field();
					}
					break;
				case 2:
					{
					setState(74);
					method();
					}
					break;
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public ModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitModifier(this);
		}
	}

	public final ModifierContext modifier() throws RecognitionException {
		ModifierContext _localctx = new ModifierContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_modifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17))) != 0) || _la==IDENTIFIER) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public FieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_field; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitField(this);
		}
	}

	public final FieldContext field() throws RecognitionException {
		FieldContext _localctx = new FieldContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_field);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(84);
					modifier();
					}
					} 
				}
				setState(89);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			setState(90);
			match(IDENTIFIER);
			setState(92);
			_la = _input.LA(1);
			if (_la==T__18) {
				{
				setState(91);
				match(T__18);
				}
			}

			setState(94);
			match(IDENTIFIER);
			setState(95);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public MethodBodyContext methodBody() {
			return getRuleContext(MethodBodyContext.class,0);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public ThrowsExceptionsContext throwsExceptions() {
			return getRuleContext(ThrowsExceptionsContext.class,0);
		}
		public MethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitMethod(this);
		}
	}

	public final MethodContext method() throws RecognitionException {
		MethodContext _localctx = new MethodContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_method);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(97);
					modifier();
					}
					} 
				}
				setState(102);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			setState(103);
			match(IDENTIFIER);
			setState(104);
			match(IDENTIFIER);
			setState(105);
			match(T__20);
			setState(107);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(106);
				identifierList(0);
				}
			}

			setState(109);
			match(T__21);
			setState(111);
			_la = _input.LA(1);
			if (_la==T__22) {
				{
				setState(110);
				throwsExceptions();
				}
			}

			setState(115);
			switch (_input.LA(1)) {
			case T__4:
				{
				setState(113);
				methodBody();
				}
				break;
			case T__19:
				{
				setState(114);
				match(T__19);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThrowsExceptionsContext extends ParserRuleContext {
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public ThrowsExceptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_throwsExceptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterThrowsExceptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitThrowsExceptions(this);
		}
	}

	public final ThrowsExceptionsContext throwsExceptions() throws RecognitionException {
		ThrowsExceptionsContext _localctx = new ThrowsExceptionsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_throwsExceptions);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			match(T__22);
			setState(118);
			identifierList(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierListContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public TerminalNode MARKER() { return getToken(JimpleParser.MARKER, 0); }
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public IdentifierListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifierList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterIdentifierList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitIdentifierList(this);
		}
	}

	public final IdentifierListContext identifierList() throws RecognitionException {
		return identifierList(0);
	}

	private IdentifierListContext identifierList(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		IdentifierListContext _localctx = new IdentifierListContext(_ctx, _parentState);
		IdentifierListContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_identifierList, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(121);
			match(IDENTIFIER);
			setState(123);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(122);
				match(MARKER);
				}
				break;
			}
			}
			_ctx.stop = _input.LT(-1);
			setState(133);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new IdentifierListContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_identifierList);
					setState(125);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(126);
					match(T__23);
					setState(127);
					match(IDENTIFIER);
					setState(129);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						setState(128);
						match(MARKER);
						}
						break;
					}
					}
					} 
				}
				setState(135);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MethodBodyContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<TerminalNode> INTEGER() { return getTokens(JimpleParser.INTEGER); }
		public TerminalNode INTEGER(int i) {
			return getToken(JimpleParser.INTEGER, i);
		}
		public MethodBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterMethodBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitMethodBody(this);
		}
	}

	public final MethodBodyContext methodBody() throws RecognitionException {
		MethodBodyContext _localctx = new MethodBodyContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_methodBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(T__4);
			setState(147); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(147);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					setState(140);
					_la = _input.LA(1);
					if (_la==T__20) {
						{
						setState(137);
						match(T__20);
						setState(138);
						match(INTEGER);
						setState(139);
						match(T__21);
						}
					}

					setState(142);
					statement();
					setState(143);
					match(T__19);
					}
					break;
				case 2:
					{
					setState(145);
					match(IDENTIFIER);
					setState(146);
					match(T__24);
					}
					break;
				}
				}
				setState(149); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (T__20 - 21)) | (1L << (T__38 - 21)) | (1L << (T__39 - 21)) | (1L << (T__40 - 21)) | (1L << (T__41 - 21)) | (1L << (T__42 - 21)) | (1L << (T__43 - 21)) | (1L << (T__47 - 21)) | (1L << (T__54 - 21)) | (1L << (T__55 - 21)) | (1L << (T__56 - 21)) | (1L << (T__59 - 21)) | (1L << (T__63 - 21)) | (1L << (T__64 - 21)) | (1L << (T__65 - 21)) | (1L << (IDENTIFIER - 21)))) != 0) );
			setState(151);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public DeclarationStmtContext declarationStmt() {
			return getRuleContext(DeclarationStmtContext.class,0);
		}
		public ComplexAssignmentStmtContext complexAssignmentStmt() {
			return getRuleContext(ComplexAssignmentStmtContext.class,0);
		}
		public AssignmentStmtContext assignmentStmt() {
			return getRuleContext(AssignmentStmtContext.class,0);
		}
		public ReturnStmtContext returnStmt() {
			return getRuleContext(ReturnStmtContext.class,0);
		}
		public InvokeStmtContext invokeStmt() {
			return getRuleContext(InvokeStmtContext.class,0);
		}
		public AllocationStmtContext allocationStmt() {
			return getRuleContext(AllocationStmtContext.class,0);
		}
		public JumpStmtContext jumpStmt() {
			return getRuleContext(JumpStmtContext.class,0);
		}
		public SwitchStmtContext switchStmt() {
			return getRuleContext(SwitchStmtContext.class,0);
		}
		public CatchStmtContext catchStmt() {
			return getRuleContext(CatchStmtContext.class,0);
		}
		public MonitorStmtContext monitorStmt() {
			return getRuleContext(MonitorStmtContext.class,0);
		}
		public NopStmtContext nopStmt() {
			return getRuleContext(NopStmtContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_statement);
		try {
			setState(164);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(153);
				declarationStmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(154);
				complexAssignmentStmt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(155);
				assignmentStmt();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(156);
				returnStmt();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(157);
				invokeStmt();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(158);
				allocationStmt();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(159);
				jumpStmt();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(160);
				switchStmt();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(161);
				catchStmt();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(162);
				monitorStmt();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(163);
				nopStmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationStmtContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public DeclarationStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarationStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterDeclarationStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitDeclarationStmt(this);
		}
	}

	public final DeclarationStmtContext declarationStmt() throws RecognitionException {
		DeclarationStmtContext _localctx = new DeclarationStmtContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_declarationStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			match(IDENTIFIER);
			setState(167);
			identifierList(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComplexAssignmentStmtContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public FieldSigContext fieldSig() {
			return getRuleContext(FieldSigContext.class,0);
		}
		public ComplexAssignmentStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complexAssignmentStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterComplexAssignmentStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitComplexAssignmentStmt(this);
		}
	}

	public final ComplexAssignmentStmtContext complexAssignmentStmt() throws RecognitionException {
		ComplexAssignmentStmtContext _localctx = new ComplexAssignmentStmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_complexAssignmentStmt);
		int _la;
		try {
			setState(189);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(169);
				match(IDENTIFIER);
				setState(170);
				match(T__25);
				setState(171);
				value();
				setState(172);
				match(T__26);
				setState(173);
				match(T__27);
				setState(174);
				value();
				setState(179);
				_la = _input.LA(1);
				if (_la==T__25) {
					{
					setState(175);
					match(T__25);
					setState(176);
					value();
					setState(177);
					match(T__26);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				_la = _input.LA(1);
				if (_la==IDENTIFIER) {
					{
					setState(181);
					match(IDENTIFIER);
					setState(182);
					match(T__28);
					}
				}

				setState(185);
				fieldSig();
				setState(186);
				match(T__27);
				setState(187);
				value();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentStmtContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public TerminalNode OP() { return getToken(JimpleParser.OP, 0); }
		public FieldSigContext fieldSig() {
			return getRuleContext(FieldSigContext.class,0);
		}
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public AssignmentStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterAssignmentStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitAssignmentStmt(this);
		}
	}

	public final AssignmentStmtContext assignmentStmt() throws RecognitionException {
		AssignmentStmtContext _localctx = new AssignmentStmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_assignmentStmt);
		int _la;
		try {
			setState(239);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(191);
				match(IDENTIFIER);
				setState(192);
				match(T__29);
				setState(193);
				match(IDENTIFIER);
				setState(194);
				match(T__24);
				setState(195);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(196);
				match(IDENTIFIER);
				setState(197);
				match(T__29);
				setState(198);
				match(T__30);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(199);
				match(IDENTIFIER);
				setState(200);
				match(T__27);
				setState(201);
				value();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(202);
				match(IDENTIFIER);
				setState(203);
				match(T__27);
				setState(204);
				match(T__20);
				setState(205);
				match(IDENTIFIER);
				setState(206);
				match(T__21);
				setState(207);
				value();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(208);
				match(IDENTIFIER);
				setState(209);
				match(T__27);
				setState(210);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__31) | (1L << T__32))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(211);
				value();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(212);
				match(IDENTIFIER);
				setState(213);
				match(T__27);
				setState(214);
				value();
				setState(215);
				_la = _input.LA(1);
				if ( !(((((_la - 34)) & ~0x3f) == 0 && ((1L << (_la - 34)) & ((1L << (T__33 - 34)) | (1L << (T__34 - 34)) | (1L << (T__35 - 34)) | (1L << (T__36 - 34)) | (1L << (OP - 34)))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(216);
				value();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(218);
				match(IDENTIFIER);
				setState(219);
				match(T__27);
				setState(220);
				value();
				setState(221);
				match(T__25);
				setState(222);
				value();
				setState(223);
				match(T__26);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(225);
				match(IDENTIFIER);
				setState(226);
				match(T__27);
				setState(229);
				_la = _input.LA(1);
				if (_la==IDENTIFIER) {
					{
					setState(227);
					match(IDENTIFIER);
					setState(228);
					match(T__28);
					}
				}

				setState(231);
				fieldSig();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(232);
				match(IDENTIFIER);
				setState(233);
				match(T__27);
				setState(234);
				match(T__37);
				setState(235);
				match(T__20);
				setState(236);
				identifierList(0);
				setState(237);
				match(T__21);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStmtContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ReturnStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterReturnStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitReturnStmt(this);
		}
	}

	public final ReturnStmtContext returnStmt() throws RecognitionException {
		ReturnStmtContext _localctx = new ReturnStmtContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_returnStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			match(T__38);
			setState(243);
			_la = _input.LA(1);
			if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
				{
				setState(242);
				value();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvokeStmtContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public MethodSigContext methodSig() {
			return getRuleContext(MethodSigContext.class,0);
		}
		public ValueListContext valueList() {
			return getRuleContext(ValueListContext.class,0);
		}
		public TerminalNode STRING() { return getToken(JimpleParser.STRING, 0); }
		public DynamicMethodSigContext dynamicMethodSig() {
			return getRuleContext(DynamicMethodSigContext.class,0);
		}
		public BootValueListContext bootValueList() {
			return getRuleContext(BootValueListContext.class,0);
		}
		public InvokeStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invokeStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterInvokeStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitInvokeStmt(this);
		}
	}

	public final InvokeStmtContext invokeStmt() throws RecognitionException {
		InvokeStmtContext _localctx = new InvokeStmtContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_invokeStmt);
		int _la;
		try {
			setState(290);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(247);
				_la = _input.LA(1);
				if (_la==IDENTIFIER) {
					{
					setState(245);
					match(IDENTIFIER);
					setState(246);
					match(T__27);
					}
				}

				setState(249);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__39) | (1L << T__40) | (1L << T__41))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(250);
				match(IDENTIFIER);
				setState(251);
				match(T__28);
				setState(252);
				methodSig();
				setState(253);
				match(T__20);
				setState(255);
				_la = _input.LA(1);
				if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
					{
					setState(254);
					valueList(0);
					}
				}

				setState(257);
				match(T__21);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(261);
				_la = _input.LA(1);
				if (_la==IDENTIFIER) {
					{
					setState(259);
					match(IDENTIFIER);
					setState(260);
					match(T__27);
					}
				}

				setState(263);
				match(T__42);
				setState(264);
				methodSig();
				setState(265);
				match(T__20);
				setState(267);
				_la = _input.LA(1);
				if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
					{
					setState(266);
					valueList(0);
					}
				}

				setState(269);
				match(T__21);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(273);
				_la = _input.LA(1);
				if (_la==IDENTIFIER) {
					{
					setState(271);
					match(IDENTIFIER);
					setState(272);
					match(T__27);
					}
				}

				setState(275);
				match(T__43);
				setState(276);
				match(STRING);
				setState(277);
				dynamicMethodSig();
				setState(278);
				match(T__20);
				setState(280);
				_la = _input.LA(1);
				if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
					{
					setState(279);
					valueList(0);
					}
				}

				setState(282);
				match(T__21);
				setState(283);
				methodSig();
				setState(284);
				match(T__20);
				setState(286);
				_la = _input.LA(1);
				if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
					{
					setState(285);
					bootValueList();
					}
				}

				setState(288);
				match(T__21);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllocationStmtContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public AllocationStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allocationStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterAllocationStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitAllocationStmt(this);
		}
	}

	public final AllocationStmtContext allocationStmt() throws RecognitionException {
		AllocationStmtContext _localctx = new AllocationStmtContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_allocationStmt);
		int _la;
		try {
			setState(324);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(292);
				match(IDENTIFIER);
				setState(293);
				match(T__27);
				setState(294);
				match(T__44);
				setState(295);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(296);
				match(IDENTIFIER);
				setState(297);
				match(T__27);
				setState(298);
				match(T__45);
				setState(299);
				match(T__20);
				setState(300);
				match(IDENTIFIER);
				setState(301);
				match(T__21);
				setState(302);
				match(T__25);
				setState(303);
				value();
				setState(304);
				match(T__26);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(306);
				match(IDENTIFIER);
				setState(307);
				match(T__27);
				setState(308);
				match(T__46);
				setState(309);
				match(T__20);
				setState(310);
				match(IDENTIFIER);
				setState(311);
				match(T__21);
				setState(317); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(312);
					match(T__25);
					setState(314);
					_la = _input.LA(1);
					if (_la==T__0 || _la==T__49 || ((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (INTEGER - 67)) | (1L << (REAL - 67)) | (1L << (STRING - 67)) | (1L << (IDENTIFIER - 67)))) != 0)) {
						{
						setState(313);
						value();
						}
					}

					setState(316);
					match(T__26);
					}
					}
					setState(319); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__25 );
				setState(322);
				_la = _input.LA(1);
				if (_la==T__18) {
					{
					setState(321);
					match(T__18);
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodSigContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public MethodSigContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodSig; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterMethodSig(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitMethodSig(this);
		}
	}

	public final MethodSigContext methodSig() throws RecognitionException {
		MethodSigContext _localctx = new MethodSigContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_methodSig);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(326);
			match(T__47);
			setState(327);
			match(IDENTIFIER);
			setState(328);
			match(T__24);
			setState(329);
			match(IDENTIFIER);
			setState(330);
			match(IDENTIFIER);
			setState(331);
			match(T__20);
			setState(333);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(332);
				identifierList(0);
				}
			}

			setState(335);
			match(T__21);
			setState(336);
			match(T__48);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DynamicMethodSigContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public DynamicMethodSigContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dynamicMethodSig; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterDynamicMethodSig(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitDynamicMethodSig(this);
		}
	}

	public final DynamicMethodSigContext dynamicMethodSig() throws RecognitionException {
		DynamicMethodSigContext _localctx = new DynamicMethodSigContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_dynamicMethodSig);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(338);
			match(T__47);
			setState(339);
			match(IDENTIFIER);
			setState(340);
			match(T__20);
			setState(342);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(341);
				identifierList(0);
				}
			}

			setState(344);
			match(T__21);
			setState(345);
			match(T__48);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldSigContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public FieldSigContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldSig; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterFieldSig(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitFieldSig(this);
		}
	}

	public final FieldSigContext fieldSig() throws RecognitionException {
		FieldSigContext _localctx = new FieldSigContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_fieldSig);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(347);
			match(T__47);
			setState(348);
			match(IDENTIFIER);
			setState(349);
			match(T__24);
			setState(350);
			match(IDENTIFIER);
			setState(351);
			match(IDENTIFIER);
			setState(352);
			match(T__48);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public TerminalNode INTEGER() { return getToken(JimpleParser.INTEGER, 0); }
		public TerminalNode REAL() { return getToken(JimpleParser.REAL, 0); }
		public TerminalNode STRING() { return getToken(JimpleParser.STRING, 0); }
		public MethodSigContext methodSig() {
			return getRuleContext(MethodSigContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_value);
		try {
			setState(362);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(354);
				match(IDENTIFIER);
				}
				break;
			case INTEGER:
				enterOuterAlt(_localctx, 2);
				{
				setState(355);
				match(INTEGER);
				}
				break;
			case REAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(356);
				match(REAL);
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 4);
				{
				setState(357);
				match(STRING);
				}
				break;
			case T__0:
				enterOuterAlt(_localctx, 5);
				{
				setState(358);
				match(T__0);
				setState(359);
				match(STRING);
				}
				break;
			case T__49:
				enterOuterAlt(_localctx, 6);
				{
				setState(360);
				match(T__49);
				setState(361);
				methodSig();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueListContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ValueListContext valueList() {
			return getRuleContext(ValueListContext.class,0);
		}
		public ValueListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterValueList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitValueList(this);
		}
	}

	public final ValueListContext valueList() throws RecognitionException {
		return valueList(0);
	}

	private ValueListContext valueList(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ValueListContext _localctx = new ValueListContext(_ctx, _parentState);
		ValueListContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_valueList, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(365);
			value();
			}
			_ctx.stop = _input.LT(-1);
			setState(372);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ValueListContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_valueList);
					setState(367);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(368);
					match(T__23);
					setState(369);
					value();
					}
					} 
				}
				setState(374);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BootValueListContext extends ParserRuleContext {
		public ValueListContext valueList() {
			return getRuleContext(ValueListContext.class,0);
		}
		public BootValueListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bootValueList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterBootValueList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitBootValueList(this);
		}
	}

	public final BootValueListContext bootValueList() throws RecognitionException {
		BootValueListContext _localctx = new BootValueListContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_bootValueList);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(375);
			valueList(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JumpStmtContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public JumpStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jumpStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterJumpStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitJumpStmt(this);
		}
	}

	public final JumpStmtContext jumpStmt() throws RecognitionException {
		JumpStmtContext _localctx = new JumpStmtContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_jumpStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(382);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(377);
				match(IDENTIFIER);
				setState(378);
				value();
				setState(379);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__47) | (1L << T__48) | (1L << T__50) | (1L << T__51) | (1L << T__52) | (1L << T__53))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(380);
				value();
				}
			}

			setState(384);
			match(T__54);
			setState(385);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchStmtContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public List<CaseStmtContext> caseStmt() {
			return getRuleContexts(CaseStmtContext.class);
		}
		public CaseStmtContext caseStmt(int i) {
			return getRuleContext(CaseStmtContext.class,i);
		}
		public SwitchStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterSwitchStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitSwitchStmt(this);
		}
	}

	public final SwitchStmtContext switchStmt() throws RecognitionException {
		SwitchStmtContext _localctx = new SwitchStmtContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_switchStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(387);
			_la = _input.LA(1);
			if ( !(_la==T__55 || _la==T__56) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(388);
			match(T__20);
			setState(389);
			value();
			setState(390);
			match(T__21);
			setState(391);
			match(T__4);
			setState(395);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__57 || _la==T__58) {
				{
				{
				setState(392);
				caseStmt();
				}
				}
				setState(397);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(398);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseStmtContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public TerminalNode INTEGER() { return getToken(JimpleParser.INTEGER, 0); }
		public CaseStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterCaseStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitCaseStmt(this);
		}
	}

	public final CaseStmtContext caseStmt() throws RecognitionException {
		CaseStmtContext _localctx = new CaseStmtContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_caseStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(403);
			switch (_input.LA(1)) {
			case T__57:
				{
				setState(400);
				match(T__57);
				setState(401);
				match(INTEGER);
				}
				break;
			case T__58:
				{
				setState(402);
				match(T__58);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(405);
			match(T__24);
			setState(406);
			match(T__54);
			setState(407);
			match(IDENTIFIER);
			setState(408);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchStmtContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(JimpleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(JimpleParser.IDENTIFIER, i);
		}
		public CatchStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterCatchStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitCatchStmt(this);
		}
	}

	public final CatchStmtContext catchStmt() throws RecognitionException {
		CatchStmtContext _localctx = new CatchStmtContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_catchStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(410);
			match(T__59);
			setState(411);
			match(IDENTIFIER);
			setState(412);
			match(T__60);
			setState(413);
			match(IDENTIFIER);
			setState(414);
			match(T__61);
			setState(415);
			match(IDENTIFIER);
			setState(416);
			match(T__62);
			setState(417);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MonitorStmtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(JimpleParser.STRING, 0); }
		public TerminalNode IDENTIFIER() { return getToken(JimpleParser.IDENTIFIER, 0); }
		public MonitorStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_monitorStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterMonitorStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitMonitorStmt(this);
		}
	}

	public final MonitorStmtContext monitorStmt() throws RecognitionException {
		MonitorStmtContext _localctx = new MonitorStmtContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_monitorStmt);
		try {
			setState(429);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(419);
				match(T__63);
				setState(420);
				match(T__0);
				setState(421);
				match(STRING);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(422);
				match(T__64);
				setState(423);
				match(T__0);
				setState(424);
				match(STRING);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(425);
				match(T__63);
				setState(426);
				match(IDENTIFIER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(427);
				match(T__64);
				setState(428);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NopStmtContext extends ParserRuleContext {
		public NopStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nopStmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).enterNopStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JimpleListener ) ((JimpleListener)listener).exitNopStmt(this);
		}
	}

	public final NopStmtContext nopStmt() throws RecognitionException {
		NopStmtContext _localctx = new NopStmtContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_nopStmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(431);
			match(T__65);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 6:
			return identifierList_sempred((IdentifierListContext)_localctx, predIndex);
		case 19:
			return valueList_sempred((ValueListContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean identifierList_sempred(IdentifierListContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean valueList_sempred(ValueListContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3L\u01b4\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\3\7\3<\n\3\f\3\16\3?\13\3\3\3"+
		"\3\3\3\3\3\3\5\3E\n\3\3\3\3\3\5\3I\n\3\3\3\3\3\3\3\7\3N\n\3\f\3\16\3Q"+
		"\13\3\3\3\3\3\3\4\3\4\3\5\7\5X\n\5\f\5\16\5[\13\5\3\5\3\5\5\5_\n\5\3\5"+
		"\3\5\3\5\3\6\7\6e\n\6\f\6\16\6h\13\6\3\6\3\6\3\6\3\6\5\6n\n\6\3\6\3\6"+
		"\5\6r\n\6\3\6\3\6\5\6v\n\6\3\7\3\7\3\7\3\b\3\b\3\b\5\b~\n\b\3\b\3\b\3"+
		"\b\3\b\5\b\u0084\n\b\7\b\u0086\n\b\f\b\16\b\u0089\13\b\3\t\3\t\3\t\3\t"+
		"\5\t\u008f\n\t\3\t\3\t\3\t\3\t\3\t\6\t\u0096\n\t\r\t\16\t\u0097\3\t\3"+
		"\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00a7\n\n\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00b6\n\f\3\f\3\f\5"+
		"\f\u00ba\n\f\3\f\3\f\3\f\3\f\5\f\u00c0\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00e8\n\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00f2\n\r\3\16\3\16\5\16\u00f6\n\16"+
		"\3\17\3\17\5\17\u00fa\n\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0102\n"+
		"\17\3\17\3\17\3\17\3\17\5\17\u0108\n\17\3\17\3\17\3\17\3\17\5\17\u010e"+
		"\n\17\3\17\3\17\3\17\3\17\5\17\u0114\n\17\3\17\3\17\3\17\3\17\3\17\5\17"+
		"\u011b\n\17\3\17\3\17\3\17\3\17\5\17\u0121\n\17\3\17\3\17\5\17\u0125\n"+
		"\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3"+
		"\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u013d\n\20\3\20\6\20"+
		"\u0140\n\20\r\20\16\20\u0141\3\20\5\20\u0145\n\20\5\20\u0147\n\20\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0150\n\21\3\21\3\21\3\21\3\22\3\22"+
		"\3\22\3\22\5\22\u0159\n\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u016d\n\24\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\7\25\u0175\n\25\f\25\16\25\u0178\13\25\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u0181\n\27\3\27\3\27\3\27\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\7\30\u018c\n\30\f\30\16\30\u018f\13\30\3\30\3\30\3\31"+
		"\3\31\3\31\5\31\u0196\n\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\5\33\u01b0\n\33\3\34\3\34\3\34\2\4\16(\35\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\60\62\64\66\2\t\3\2\3\4\4\2\t\24JJ\4\2\3\3\""+
		"#\4\2$\'KK\3\2*,\4\2\62\63\658\3\2:;\u01dc\28\3\2\2\2\4=\3\2\2\2\6T\3"+
		"\2\2\2\bY\3\2\2\2\nf\3\2\2\2\fw\3\2\2\2\16z\3\2\2\2\20\u008a\3\2\2\2\22"+
		"\u00a6\3\2\2\2\24\u00a8\3\2\2\2\26\u00bf\3\2\2\2\30\u00f1\3\2\2\2\32\u00f3"+
		"\3\2\2\2\34\u0124\3\2\2\2\36\u0146\3\2\2\2 \u0148\3\2\2\2\"\u0154\3\2"+
		"\2\2$\u015d\3\2\2\2&\u016c\3\2\2\2(\u016e\3\2\2\2*\u0179\3\2\2\2,\u0180"+
		"\3\2\2\2.\u0185\3\2\2\2\60\u0195\3\2\2\2\62\u019c\3\2\2\2\64\u01af\3\2"+
		"\2\2\66\u01b1\3\2\2\289\5\4\3\29\3\3\2\2\2:<\5\6\4\2;:\3\2\2\2<?\3\2\2"+
		"\2=;\3\2\2\2=>\3\2\2\2>@\3\2\2\2?=\3\2\2\2@A\t\2\2\2AD\7J\2\2BC\7\5\2"+
		"\2CE\7J\2\2DB\3\2\2\2DE\3\2\2\2EH\3\2\2\2FG\7\6\2\2GI\5\16\b\2HF\3\2\2"+
		"\2HI\3\2\2\2IJ\3\2\2\2JO\7\7\2\2KN\5\b\5\2LN\5\n\6\2MK\3\2\2\2ML\3\2\2"+
		"\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2\2RS\7\b\2\2S\5\3\2"+
		"\2\2TU\t\3\2\2U\7\3\2\2\2VX\5\6\4\2WV\3\2\2\2X[\3\2\2\2YW\3\2\2\2YZ\3"+
		"\2\2\2Z\\\3\2\2\2[Y\3\2\2\2\\^\7J\2\2]_\7\25\2\2^]\3\2\2\2^_\3\2\2\2_"+
		"`\3\2\2\2`a\7J\2\2ab\7\26\2\2b\t\3\2\2\2ce\5\6\4\2dc\3\2\2\2eh\3\2\2\2"+
		"fd\3\2\2\2fg\3\2\2\2gi\3\2\2\2hf\3\2\2\2ij\7J\2\2jk\7J\2\2km\7\27\2\2"+
		"ln\5\16\b\2ml\3\2\2\2mn\3\2\2\2no\3\2\2\2oq\7\30\2\2pr\5\f\7\2qp\3\2\2"+
		"\2qr\3\2\2\2ru\3\2\2\2sv\5\20\t\2tv\7\26\2\2us\3\2\2\2ut\3\2\2\2v\13\3"+
		"\2\2\2wx\7\31\2\2xy\5\16\b\2y\r\3\2\2\2z{\b\b\1\2{}\7J\2\2|~\7F\2\2}|"+
		"\3\2\2\2}~\3\2\2\2~\u0087\3\2\2\2\177\u0080\f\3\2\2\u0080\u0081\7\32\2"+
		"\2\u0081\u0083\7J\2\2\u0082\u0084\7F\2\2\u0083\u0082\3\2\2\2\u0083\u0084"+
		"\3\2\2\2\u0084\u0086\3\2\2\2\u0085\177\3\2\2\2\u0086\u0089\3\2\2\2\u0087"+
		"\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088\17\3\2\2\2\u0089\u0087\3\2\2"+
		"\2\u008a\u0095\7\7\2\2\u008b\u008c\7\27\2\2\u008c\u008d\7E\2\2\u008d\u008f"+
		"\7\30\2\2\u008e\u008b\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\3\2\2\2"+
		"\u0090\u0091\5\22\n\2\u0091\u0092\7\26\2\2\u0092\u0096\3\2\2\2\u0093\u0094"+
		"\7J\2\2\u0094\u0096\7\33\2\2\u0095\u008e\3\2\2\2\u0095\u0093\3\2\2\2\u0096"+
		"\u0097\3\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u0099\3\2"+
		"\2\2\u0099\u009a\7\b\2\2\u009a\21\3\2\2\2\u009b\u00a7\5\24\13\2\u009c"+
		"\u00a7\5\26\f\2\u009d\u00a7\5\30\r\2\u009e\u00a7\5\32\16\2\u009f\u00a7"+
		"\5\34\17\2\u00a0\u00a7\5\36\20\2\u00a1\u00a7\5,\27\2\u00a2\u00a7\5.\30"+
		"\2\u00a3\u00a7\5\62\32\2\u00a4\u00a7\5\64\33\2\u00a5\u00a7\5\66\34\2\u00a6"+
		"\u009b\3\2\2\2\u00a6\u009c\3\2\2\2\u00a6\u009d\3\2\2\2\u00a6\u009e\3\2"+
		"\2\2\u00a6\u009f\3\2\2\2\u00a6\u00a0\3\2\2\2\u00a6\u00a1\3\2\2\2\u00a6"+
		"\u00a2\3\2\2\2\u00a6\u00a3\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a5\3\2"+
		"\2\2\u00a7\23\3\2\2\2\u00a8\u00a9\7J\2\2\u00a9\u00aa\5\16\b\2\u00aa\25"+
		"\3\2\2\2\u00ab\u00ac\7J\2\2\u00ac\u00ad\7\34\2\2\u00ad\u00ae\5&\24\2\u00ae"+
		"\u00af\7\35\2\2\u00af\u00b0\7\36\2\2\u00b0\u00b5\5&\24\2\u00b1\u00b2\7"+
		"\34\2\2\u00b2\u00b3\5&\24\2\u00b3\u00b4\7\35\2\2\u00b4\u00b6\3\2\2\2\u00b5"+
		"\u00b1\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00c0\3\2\2\2\u00b7\u00b8\7J"+
		"\2\2\u00b8\u00ba\7\37\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba"+
		"\u00bb\3\2\2\2\u00bb\u00bc\5$\23\2\u00bc\u00bd\7\36\2\2\u00bd\u00be\5"+
		"&\24\2\u00be\u00c0\3\2\2\2\u00bf\u00ab\3\2\2\2\u00bf\u00b9\3\2\2\2\u00c0"+
		"\27\3\2\2\2\u00c1\u00c2\7J\2\2\u00c2\u00c3\7 \2\2\u00c3\u00c4\7J\2\2\u00c4"+
		"\u00c5\7\33\2\2\u00c5\u00f2\7J\2\2\u00c6\u00c7\7J\2\2\u00c7\u00c8\7 \2"+
		"\2\u00c8\u00f2\7!\2\2\u00c9\u00ca\7J\2\2\u00ca\u00cb\7\36\2\2\u00cb\u00f2"+
		"\5&\24\2\u00cc\u00cd\7J\2\2\u00cd\u00ce\7\36\2\2\u00ce\u00cf\7\27\2\2"+
		"\u00cf\u00d0\7J\2\2\u00d0\u00d1\7\30\2\2\u00d1\u00f2\5&\24\2\u00d2\u00d3"+
		"\7J\2\2\u00d3\u00d4\7\36\2\2\u00d4\u00d5\t\4\2\2\u00d5\u00f2\5&\24\2\u00d6"+
		"\u00d7\7J\2\2\u00d7\u00d8\7\36\2\2\u00d8\u00d9\5&\24\2\u00d9\u00da\t\5"+
		"\2\2\u00da\u00db\5&\24\2\u00db\u00f2\3\2\2\2\u00dc\u00dd\7J\2\2\u00dd"+
		"\u00de\7\36\2\2\u00de\u00df\5&\24\2\u00df\u00e0\7\34\2\2\u00e0\u00e1\5"+
		"&\24\2\u00e1\u00e2\7\35\2\2\u00e2\u00f2\3\2\2\2\u00e3\u00e4\7J\2\2\u00e4"+
		"\u00e7\7\36\2\2\u00e5\u00e6\7J\2\2\u00e6\u00e8\7\37\2\2\u00e7\u00e5\3"+
		"\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00f2\5$\23\2\u00ea"+
		"\u00eb\7J\2\2\u00eb\u00ec\7\36\2\2\u00ec\u00ed\7(\2\2\u00ed\u00ee\7\27"+
		"\2\2\u00ee\u00ef\5\16\b\2\u00ef\u00f0\7\30\2\2\u00f0\u00f2\3\2\2\2\u00f1"+
		"\u00c1\3\2\2\2\u00f1\u00c6\3\2\2\2\u00f1\u00c9\3\2\2\2\u00f1\u00cc\3\2"+
		"\2\2\u00f1\u00d2\3\2\2\2\u00f1\u00d6\3\2\2\2\u00f1\u00dc\3\2\2\2\u00f1"+
		"\u00e3\3\2\2\2\u00f1\u00ea\3\2\2\2\u00f2\31\3\2\2\2\u00f3\u00f5\7)\2\2"+
		"\u00f4\u00f6\5&\24\2\u00f5\u00f4\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\33"+
		"\3\2\2\2\u00f7\u00f8\7J\2\2\u00f8\u00fa\7\36\2\2\u00f9\u00f7\3\2\2\2\u00f9"+
		"\u00fa\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fc\t\6\2\2\u00fc\u00fd\7J"+
		"\2\2\u00fd\u00fe\7\37\2\2\u00fe\u00ff\5 \21\2\u00ff\u0101\7\27\2\2\u0100"+
		"\u0102\5(\25\2\u0101\u0100\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0103\3\2"+
		"\2\2\u0103\u0104\7\30\2\2\u0104\u0125\3\2\2\2\u0105\u0106\7J\2\2\u0106"+
		"\u0108\7\36\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2\u0108\u0109\3"+
		"\2\2\2\u0109\u010a\7-\2\2\u010a\u010b\5 \21\2\u010b\u010d\7\27\2\2\u010c"+
		"\u010e\5(\25\2\u010d\u010c\3\2\2\2\u010d\u010e\3\2\2\2\u010e\u010f\3\2"+
		"\2\2\u010f\u0110\7\30\2\2\u0110\u0125\3\2\2\2\u0111\u0112\7J\2\2\u0112"+
		"\u0114\7\36\2\2\u0113\u0111\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\3"+
		"\2\2\2\u0115\u0116\7.\2\2\u0116\u0117\7I\2\2\u0117\u0118\5\"\22\2\u0118"+
		"\u011a\7\27\2\2\u0119\u011b\5(\25\2\u011a\u0119\3\2\2\2\u011a\u011b\3"+
		"\2\2\2\u011b\u011c\3\2\2\2\u011c\u011d\7\30\2\2\u011d\u011e\5 \21\2\u011e"+
		"\u0120\7\27\2\2\u011f\u0121\5*\26\2\u0120\u011f\3\2\2\2\u0120\u0121\3"+
		"\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\7\30\2\2\u0123\u0125\3\2\2\2\u0124"+
		"\u00f9\3\2\2\2\u0124\u0107\3\2\2\2\u0124\u0113\3\2\2\2\u0125\35\3\2\2"+
		"\2\u0126\u0127\7J\2\2\u0127\u0128\7\36\2\2\u0128\u0129\7/\2\2\u0129\u0147"+
		"\7J\2\2\u012a\u012b\7J\2\2\u012b\u012c\7\36\2\2\u012c\u012d\7\60\2\2\u012d"+
		"\u012e\7\27\2\2\u012e\u012f\7J\2\2\u012f\u0130\7\30\2\2\u0130\u0131\7"+
		"\34\2\2\u0131\u0132\5&\24\2\u0132\u0133\7\35\2\2\u0133\u0147\3\2\2\2\u0134"+
		"\u0135\7J\2\2\u0135\u0136\7\36\2\2\u0136\u0137\7\61\2\2\u0137\u0138\7"+
		"\27\2\2\u0138\u0139\7J\2\2\u0139\u013f\7\30\2\2\u013a\u013c\7\34\2\2\u013b"+
		"\u013d\5&\24\2\u013c\u013b\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013e\3\2"+
		"\2\2\u013e\u0140\7\35\2\2\u013f\u013a\3\2\2\2\u0140\u0141\3\2\2\2\u0141"+
		"\u013f\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0144\3\2\2\2\u0143\u0145\7\25"+
		"\2\2\u0144\u0143\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u0147\3\2\2\2\u0146"+
		"\u0126\3\2\2\2\u0146\u012a\3\2\2\2\u0146\u0134\3\2\2\2\u0147\37\3\2\2"+
		"\2\u0148\u0149\7\62\2\2\u0149\u014a\7J\2\2\u014a\u014b\7\33\2\2\u014b"+
		"\u014c\7J\2\2\u014c\u014d\7J\2\2\u014d\u014f\7\27\2\2\u014e\u0150\5\16"+
		"\b\2\u014f\u014e\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151\3\2\2\2\u0151"+
		"\u0152\7\30\2\2\u0152\u0153\7\63\2\2\u0153!\3\2\2\2\u0154\u0155\7\62\2"+
		"\2\u0155\u0156\7J\2\2\u0156\u0158\7\27\2\2\u0157\u0159\5\16\b\2\u0158"+
		"\u0157\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\7\30"+
		"\2\2\u015b\u015c\7\63\2\2\u015c#\3\2\2\2\u015d\u015e\7\62\2\2\u015e\u015f"+
		"\7J\2\2\u015f\u0160\7\33\2\2\u0160\u0161\7J\2\2\u0161\u0162\7J\2\2\u0162"+
		"\u0163\7\63\2\2\u0163%\3\2\2\2\u0164\u016d\7J\2\2\u0165\u016d\7E\2\2\u0166"+
		"\u016d\7G\2\2\u0167\u016d\7I\2\2\u0168\u0169\7\3\2\2\u0169\u016d\7I\2"+
		"\2\u016a\u016b\7\64\2\2\u016b\u016d\5 \21\2\u016c\u0164\3\2\2\2\u016c"+
		"\u0165\3\2\2\2\u016c\u0166\3\2\2\2\u016c\u0167\3\2\2\2\u016c\u0168\3\2"+
		"\2\2\u016c\u016a\3\2\2\2\u016d\'\3\2\2\2\u016e\u016f\b\25\1\2\u016f\u0170"+
		"\5&\24\2\u0170\u0176\3\2\2\2\u0171\u0172\f\3\2\2\u0172\u0173\7\32\2\2"+
		"\u0173\u0175\5&\24\2\u0174\u0171\3\2\2\2\u0175\u0178\3\2\2\2\u0176\u0174"+
		"\3\2\2\2\u0176\u0177\3\2\2\2\u0177)\3\2\2\2\u0178\u0176\3\2\2\2\u0179"+
		"\u017a\5(\25\2\u017a+\3\2\2\2\u017b\u017c\7J\2\2\u017c\u017d\5&\24\2\u017d"+
		"\u017e\t\7\2\2\u017e\u017f\5&\24\2\u017f\u0181\3\2\2\2\u0180\u017b\3\2"+
		"\2\2\u0180\u0181\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0183\79\2\2\u0183"+
		"\u0184\7J\2\2\u0184-\3\2\2\2\u0185\u0186\t\b\2\2\u0186\u0187\7\27\2\2"+
		"\u0187\u0188\5&\24\2\u0188\u0189\7\30\2\2\u0189\u018d\7\7\2\2\u018a\u018c"+
		"\5\60\31\2\u018b\u018a\3\2\2\2\u018c\u018f\3\2\2\2\u018d\u018b\3\2\2\2"+
		"\u018d\u018e\3\2\2\2\u018e\u0190\3\2\2\2\u018f\u018d\3\2\2\2\u0190\u0191"+
		"\7\b\2\2\u0191/\3\2\2\2\u0192\u0193\7<\2\2\u0193\u0196\7E\2\2\u0194\u0196"+
		"\7=\2\2\u0195\u0192\3\2\2\2\u0195\u0194\3\2\2\2\u0196\u0197\3\2\2\2\u0197"+
		"\u0198\7\33\2\2\u0198\u0199\79\2\2\u0199\u019a\7J\2\2\u019a\u019b\7\26"+
		"\2\2\u019b\61\3\2\2\2\u019c\u019d\7>\2\2\u019d\u019e\7J\2\2\u019e\u019f"+
		"\7?\2\2\u019f\u01a0\7J\2\2\u01a0\u01a1\7@\2\2\u01a1\u01a2\7J\2\2\u01a2"+
		"\u01a3\7A\2\2\u01a3\u01a4\7J\2\2\u01a4\63\3\2\2\2\u01a5\u01a6\7B\2\2\u01a6"+
		"\u01a7\7\3\2\2\u01a7\u01b0\7I\2\2\u01a8\u01a9\7C\2\2\u01a9\u01aa\7\3\2"+
		"\2\u01aa\u01b0\7I\2\2\u01ab\u01ac\7B\2\2\u01ac\u01b0\7J\2\2\u01ad\u01ae"+
		"\7C\2\2\u01ae\u01b0\7J\2\2\u01af\u01a5\3\2\2\2\u01af\u01a8\3\2\2\2\u01af"+
		"\u01ab\3\2\2\2\u01af\u01ad\3\2\2\2\u01b0\65\3\2\2\2\u01b1\u01b2\7D\2\2"+
		"\u01b2\67\3\2\2\2.=DHMOY^fmqu}\u0083\u0087\u008e\u0095\u0097\u00a6\u00b5"+
		"\u00b9\u00bf\u00e7\u00f1\u00f5\u00f9\u0101\u0107\u010d\u0113\u011a\u0120"+
		"\u0124\u013c\u0141\u0144\u0146\u014f\u0158\u016c\u0176\u0180\u018d\u0195"+
		"\u01af";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}