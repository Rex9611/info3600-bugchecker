//If you want to test this on a different program - copy file to the output folder in doop

.type String

.symbol_type Var
.symbol_type Alloc
.number_type Index
.number_type LineNumber
.symbol_type Context
.symbol_type Instruction
.symbol_type Method
.symbol_type Signature
.symbol_type Type
.symbol_type NPEType
.symbol_type File

//=========EDM============

.decl Primitive(type: Type)
Primitive("boolean").
Primitive("short").
Primitive("int").
Primitive("long").
Primitive("float").
Primitive("double").
Primitive("char").
Primitive("byte").


.decl InstructionLine(m: Method, i: Index, l: LineNumber, f: File)
.input InstructionLine(IO="file", filename="facts/Instruction-Line.facts", delimiter="\t")
.output InstructionLine(IO=stdout)



.decl VarPointsTo(a: Alloc, v: Var)
.input VarPointsTo(IO=file, filename="database/Stats_Simple_InsensVarPointsTo.csv")

.decl CallGraphEdge(ctx: Context, ins: Instruction, hctx: Context, sig: Signature)
.input CallGraphEdge(IO=file, filename="database/CallGraphEdge.csv")
.output CallGraphEdge(IO=stdout)

.decl Reachable(m: Method)
.input Reachable(IO=file, filename="database/Reachable.csv")
.output Reachable(IO=stdout)



.decl SpecialMethodInvocation(?instruction:symbol, i: Index, sig: Signature, ?base:symbol, m: Method)
.input SpecialMethodInvocation(IO="file", filename="facts/SpecialMethodInvocation.facts", delimiter="\t")
.output SpecialMethodInvocation(IO=stdout)

.decl LoadArrayIndex(ins: Instruction, i: Index, to: Var, base: Var, m: Method)
.output LoadArrayIndex(IO=stdout)
.input LoadArrayIndex(IO="file", filename="facts/LoadArrayIndex.facts", delimiter="\t")

.decl StoreArrayIndex(ins: Instruction, i: Index, from: Var, base: Var, m: Method)
.input StoreArrayIndex(IO="file", filename="facts/StoreArrayIndex.facts", delimiter="\t")

.decl StoreInstanceField(ins: Instruction, i: Index, from: Var, base: Var, sig: Signature, m: Method)
.input StoreInstanceField(IO="file", filename="facts/StoreInstanceField.facts", delimiter="\t")

.decl LoadInstanceField(ins: Instruction, i: Index, to: Var, base: Var, sig: Signature, m: Method)
.input LoadInstanceField(IO="file", filename="facts/LoadInstanceField.facts", delimiter="\t")

.decl VirtualMethodInvocation(ins: Instruction, i: Index, sig: Signature,  base: Var, m: Method)
.input VirtualMethodInvocation(IO="file", filename="facts/VirtualMethodInvocation.facts", delimiter="\t")

.decl ThrowNull(ins: Instruction, i: Index, m: Method)
.input ThrowNull(IO="file", filename="facts/ThrowNull.facts", delimiter="\t")

.decl LoadStaticField(ins: Instruction, i: Index, to: Var, sig: Signature, m: Method)
.input LoadStaticField(IO="file", filename="facts/LoadStaticField.facts", delimiter="\t")

.decl StoreStaticField(ins: Instruction, i: Index, from: Var, sig: Signature, m: Method)
.input StoreStaticField(IO="file", filename="facts/StoreStaticField.facts", delimiter="\t")

.decl AssignCastNull(ins: Instruction, i: Index, to: Var, t: Type, m: Method)
.input AssignCastNull(IO="file", filename="facts/AssignCastNull.facts", delimiter="\t")

.decl AssignUnop(ins: Instruction, i: Index, to: Var, m: Method)
.input AssignUnop(IO="file", filename="facts/AssignUnop.facts", delimiter="\t")

.decl AssignBinop(ins: Instruction, i: Index, to: Var, m: Method)
.input AssignBinop(IO="file", filename="facts/AssignBinop.facts", delimiter="\t")

.decl AssignOperFrom(ins: Instruction, from: Var)
.input AssignOperFrom(IO="file", filename="facts/AssignOperFrom.facts", delimiter="\t")

.decl Var_Type(var: Var, type: Type)
.input Var_Type(IO="file", filename="facts/Var-Type.facts", delimiter="\t")

.decl EnterMonitor(ins: Instruction, i: Index, to: Var, m: Method)
.input EnterMonitor(IO="file", filename="facts/EnterMonitor.facts", delimiter="\t")

.decl ExitMonitor(ins: Instruction, i: Index, to: Var, m: Method)
.input ExitMonitor(IO="file", filename="facts/ExitMonitor.facts", delimiter="\t")


//=========IDB============

.decl VarPointsToNull(v: Var)
.output VarPointsToNull(IO=stdout)

.decl NullAt(m: Method, i: Index, type: NPEType)
.output NullAt(IO=stdout)

.decl ReachableNullAt(m: Method, i: Index, type: NPEType)
.output ReachableNullAt(IO=stdout)

.decl ReachableNullAtLine(m: Method, i: Index, f: File, l: LineNumber, type: NPEType)
.output ReachableNullAtLine(IO=stdout)


VarPointsToNull(var) :- VarPointsTo(alloc, var),
						alloc = "<<null pseudo heap>>".

//Casting a Null and assign it to var
VarPointsToNull(var) :- AssignCastNull(_,_,var,_,_).



NullAt(meth, index, "Throw NullPointerException") :-
CallGraphEdge(_, a, _, b),
contains("java.lang.NullPointerException", a),
SpecialMethodInvocation(a, index, b, _, meth).


NullAt(meth, index, "Load Array Index") :-
VarPointsToNull(var),
LoadArrayIndex(_, index, _, var, meth).

NullAt(meth, index, "Load Array Index") :-
!VarPointsTo(_,var),
LoadArrayIndex(_, index, _, var, meth).


NullAt(meth, index, "Store Array Index") :-
VarPointsToNull(var),
StoreArrayIndex(_, index, _, var, meth).

NullAt(meth, index, "Store Array Index") :-
!VarPointsTo(_,var),
StoreArrayIndex(_, index, _, var, meth).


NullAt(meth, index, "Store Instance Field") :-
VarPointsToNull(var),
StoreInstanceField(_, index, _, var, _, meth),
!StoreArrayIndex(_, _, _, var, meth).

NullAt(meth, index, "Store Instance Field") :-
!VarPointsTo(_,var),
StoreInstanceField(_, index, _, var, _, meth),
!StoreArrayIndex(_, _, _, var, meth).


NullAt(meth, index, "Load Instance Field") :-
VarPointsToNull(var),
LoadInstanceField(_, index, _, var, _, meth),
!LoadArrayIndex(_, _, _, var, meth).

NullAt(meth, index, "Load Instance Field") :-
!VarPointsTo(_,var),
LoadInstanceField(_, index, _, var, _, meth),
!LoadArrayIndex(_, _, _, var, meth).



NullAt(meth, index, "Virtual Method Invocation") :-
VarPointsToNull(var),
VirtualMethodInvocation(_, index, _, var, meth).

NullAt(meth, index, "Virtual Method Invocation") :-
!VarPointsTo(_,var),
VirtualMethodInvocation(_, index, _, var, meth).


NullAt(meth, index, "Unary Operator") :-
VarPointsToNull(var),
AssignUnop(ins, index, _, meth),
AssignOperFrom(ins, var).

NullAt(meth, index, "Unary Operator") :-
!VarPointsTo(_,var),
AssignUnop(ins, index, _, meth),
AssignOperFrom(ins, var),
Var_Type(var, type),
!Primitive(type).

NullAt(meth, index, "Binary Operator") :-
VarPointsToNull(var),
AssignBinop(ins, index, _, meth),
AssignOperFrom(ins, var).

NullAt(meth, index, "Binary Operator") :-
!VarPointsTo(_,var),
AssignBinop(ins, index, _, meth),
AssignOperFrom(ins, var),
Var_Type(var, type),
!Primitive(type).

NullAt(meth, index, "Throw Null") :-
ThrowNull(_, index, meth).

NullAt(meth, index, "Enter Monitor (Synchronized)") :-
VarPointsToNull(var),
EnterMonitor(_, index, var, meth).

NullAt(meth, index, "Enter Monitor (Synchronized)") :-
!VarPointsTo(_,var),
Var_Type(var, type),
!Primitive(type),
EnterMonitor(_, index, var, meth).

//Only detects Null Pointer for reachable methods
ReachableNullAt(meth, index, type) :- NullAt(meth, index, type), Reachable(meth).

ReachableNullAtLine(meth, index, file, line, type) :- 
ReachableNullAt(meth, index, type), 
InstructionLine(meth, index, line, file).
