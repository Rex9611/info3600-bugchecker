
#include "souffle/CompiledSouffle.h"

extern "C" {
}

namespace souffle {
using namespace ram;
struct t_brie_1__0__1 {
using t_ind_0 = Trie<1>;
t_ind_0 ind_0;
using t_tuple = t_ind_0::entry_type;
class iterator_0 : public std::iterator<std::forward_iterator_tag, t_tuple> {
    using nested_iterator = typename t_ind_0::iterator;
    nested_iterator nested;
    t_tuple value;
public:
    iterator_0() = default;
    iterator_0(const nested_iterator& iter) : nested(iter), value(orderOut_0(*iter)) {}
    iterator_0(const iterator_0& other) = default;
    iterator_0& operator=(const iterator_0& other) = default;
    bool operator==(const iterator_0& other) const {
        return nested == other.nested;
    }
    bool operator!=(const iterator_0& other) const {
        return !(*this == other);
    }
    const t_tuple& operator*() const {
        return value;
    }
    const t_tuple* operator->() const {
        return &value;
    }
    iterator_0& operator++() {
        ++nested;
        value = orderOut_0(*nested);
        return *this;
    }
};
using iterator = iterator_0;
struct context {
t_ind_0::op_context hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(orderIn_0(t), h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_brie_1__0__1& other) {
ind_0.insertAll(other.ind_0);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(orderIn_0(t), h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<iterator_0> equalRange_1(const t_tuple& t, context& h) const {
auto r = ind_0.template getBoundaries<1>(orderIn_0(t), h.hints_0);
return make_range(iterator_0(r.begin()), iterator_0(r.end()));
}
range<iterator_0> equalRange_1(const t_tuple& t) const {
context h; return equalRange_1(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.partition(10000)) {
    res.push_back(make_range(iterator(cur.begin()), iterator(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return iterator_0(ind_0.begin());
}
iterator end() const {
return iterator_0(ind_0.end());
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 brie index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Range-query: " << stats_0.get_boundaries.getHits() << "/" << stats_0.get_boundaries.getMisses() << "/" << stats_0.get_boundaries.getAccesses() << "\n";
}
static t_tuple orderIn_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
return res;
}
static t_tuple orderOut_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
return res;
}
};
struct t_btree_4__0_1_2_3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_1_2_3_4 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_1_2_3_4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,1,2,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_brie_2__0_1__3 {
using t_ind_0 = Trie<2>;
t_ind_0 ind_0;
using t_tuple = t_ind_0::entry_type;
class iterator_0 : public std::iterator<std::forward_iterator_tag, t_tuple> {
    using nested_iterator = typename t_ind_0::iterator;
    nested_iterator nested;
    t_tuple value;
public:
    iterator_0() = default;
    iterator_0(const nested_iterator& iter) : nested(iter), value(orderOut_0(*iter)) {}
    iterator_0(const iterator_0& other) = default;
    iterator_0& operator=(const iterator_0& other) = default;
    bool operator==(const iterator_0& other) const {
        return nested == other.nested;
    }
    bool operator!=(const iterator_0& other) const {
        return !(*this == other);
    }
    const t_tuple& operator*() const {
        return value;
    }
    const t_tuple* operator->() const {
        return &value;
    }
    iterator_0& operator++() {
        ++nested;
        value = orderOut_0(*nested);
        return *this;
    }
};
using iterator = iterator_0;
struct context {
t_ind_0::op_context hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(orderIn_0(t), h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[2];
std::copy(ramDomain, ramDomain + 2, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_brie_2__0_1__3& other) {
ind_0.insertAll(other.ind_0);
}
bool insert(RamDomain a0,RamDomain a1) {
RamDomain data[2] = {a0,a1};
return insert(data);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(orderIn_0(t), h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(orderIn_0(t), h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<iterator_0> equalRange_3(const t_tuple& t, context& h) const {
auto r = ind_0.template getBoundaries<2>(orderIn_0(t), h.hints_0);
return make_range(iterator_0(r.begin()), iterator_0(r.end()));
}
range<iterator_0> equalRange_3(const t_tuple& t) const {
context h; return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.partition(10000)) {
    res.push_back(make_range(iterator(cur.begin()), iterator(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return iterator_0(ind_0.begin());
}
iterator end() const {
return iterator_0(ind_0.end());
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 2 brie index [0,1]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Range-query: " << stats_0.get_boundaries.getHits() << "/" << stats_0.get_boundaries.getMisses() << "/" << stats_0.get_boundaries.getAccesses() << "\n";
}
static t_tuple orderIn_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
res[1] = t[1];
return res;
}
static t_tuple orderOut_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
res[1] = t[1];
return res;
}
};
struct t_btree_4__2_0_1_3__4 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<2,0,1,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__2_0_1_3__4& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_4(const t_tuple& t) const {
context h;
return equalRange_4(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [2,0,1,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__0_1_2_3__3 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__0_1_2_3__3& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_3(const t_tuple& t) const {
context h;
return equalRange_3(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [0,1,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__3_4_0_1_2__8__24 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,4,0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_4_0_1_2__8__24& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
range<t_ind_0::iterator> equalRange_24(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_24(const t_tuple& t) const {
context h;
return equalRange_24(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,4,0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_6__3_0_1_2_4_5__8 {
using t_tuple = Tuple<RamDomain, 6>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4,5>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[6];
std::copy(ramDomain, ramDomain + 6, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4,RamDomain a5) {
RamDomain data[6] = {a0,a1,a2,a3,a4,a5};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_6__3_0_1_2_4_5__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
low[5] = MIN_RAM_DOMAIN;
high[5] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 6 direct b-tree index [3,0,1,2,4,5]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_5__0_2_1_3_4__3_0_1_2_4__5__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,2,1,3,4>>;
t_ind_0 ind_0;
using t_ind_1 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_1 ind_1;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
t_ind_1::operation_hints hints_1;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
ind_1.insert(t, h.hints_1);
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__0_2_1_3_4__3_0_1_2_4__5__8& other) {
ind_0.insertAll(other.ind_0);
ind_1.insertAll(other.ind_1);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_5(const t_tuple& t) const {
context h;
return equalRange_5(t, h);
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_1.lower_bound(low, h.hints_1), ind_1.upper_bound(high, h.hints_1));
}
range<t_ind_1::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
ind_1.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [0,2,1,3,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
const auto& stats_1 = ind_1.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_1.inserts.getHits() << "/" << stats_1.inserts.getMisses() << "/" << stats_1.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_1.contains.getHits() << "/" << stats_1.contains.getMisses() << "/" << stats_1.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_1.lower_bound.getHits() << "/" << stats_1.lower_bound.getMisses() << "/" << stats_1.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_1.upper_bound.getHits() << "/" << stats_1.upper_bound.getMisses() << "/" << stats_1.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_3__0_1_2 {
using t_tuple = Tuple<RamDomain, 3>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<0,1,2>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[3];
std::copy(ramDomain, ramDomain + 3, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2) {
RamDomain data[3] = {a0,a1,a2};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_3__0_1_2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 3 direct b-tree index [0,1,2]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_btree_4__1_0_2_3__2 {
using t_tuple = Tuple<RamDomain, 4>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<1,0,2,3>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[4];
std::copy(ramDomain, ramDomain + 4, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3) {
RamDomain data[4] = {a0,a1,a2,a3};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_4__1_0_2_3__2& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[3] = MIN_RAM_DOMAIN;
high[3] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_2(const t_tuple& t) const {
context h;
return equalRange_2(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 4 direct b-tree index [1,0,2,3]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};
struct t_brie_1__0 {
using t_ind_0 = Trie<1>;
t_ind_0 ind_0;
using t_tuple = t_ind_0::entry_type;
class iterator_0 : public std::iterator<std::forward_iterator_tag, t_tuple> {
    using nested_iterator = typename t_ind_0::iterator;
    nested_iterator nested;
    t_tuple value;
public:
    iterator_0() = default;
    iterator_0(const nested_iterator& iter) : nested(iter), value(orderOut_0(*iter)) {}
    iterator_0(const iterator_0& other) = default;
    iterator_0& operator=(const iterator_0& other) = default;
    bool operator==(const iterator_0& other) const {
        return nested == other.nested;
    }
    bool operator!=(const iterator_0& other) const {
        return !(*this == other);
    }
    const t_tuple& operator*() const {
        return value;
    }
    const t_tuple* operator->() const {
        return &value;
    }
    iterator_0& operator++() {
        ++nested;
        value = orderOut_0(*nested);
        return *this;
    }
};
using iterator = iterator_0;
struct context {
t_ind_0::op_context hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(orderIn_0(t), h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[1];
std::copy(ramDomain, ramDomain + 1, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_brie_1__0& other) {
ind_0.insertAll(other.ind_0);
}
bool insert(RamDomain a0) {
RamDomain data[1] = {a0};
return insert(data);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(orderIn_0(t), h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
std::vector<range<iterator>> res;
for (const auto& cur : ind_0.partition(10000)) {
    res.push_back(make_range(iterator(cur.begin()), iterator(cur.end())));
}
return res;
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return iterator_0(ind_0.begin());
}
iterator end() const {
return iterator_0(ind_0.end());
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 1 brie index [0]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Range-query: " << stats_0.get_boundaries.getHits() << "/" << stats_0.get_boundaries.getMisses() << "/" << stats_0.get_boundaries.getAccesses() << "\n";
}
static t_tuple orderIn_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
return res;
}
static t_tuple orderOut_0(const t_tuple& t) {
t_tuple res;
res[0] = t[0];
return res;
}
};
struct t_btree_5__3_0_1_2_4__8 {
using t_tuple = Tuple<RamDomain, 5>;
using t_ind_0 = btree_set<t_tuple, index_utils::comparator<3,0,1,2,4>>;
t_ind_0 ind_0;
using iterator = t_ind_0::iterator;
struct context {
t_ind_0::operation_hints hints_0;
};
context createContext() { return context(); }
bool insert(const t_tuple& t) {
context h;
return insert(t, h);
}
bool insert(const t_tuple& t, context& h) {
if (ind_0.insert(t, h.hints_0)) {
return true;
} else return false;
}
bool insert(const RamDomain* ramDomain) {
RamDomain data[5];
std::copy(ramDomain, ramDomain + 5, data);
const t_tuple& tuple = reinterpret_cast<const t_tuple&>(data);
context h;
return insert(tuple, h);
}
bool insert(RamDomain a0,RamDomain a1,RamDomain a2,RamDomain a3,RamDomain a4) {
RamDomain data[5] = {a0,a1,a2,a3,a4};
return insert(data);
}
template <typename T>
void insertAll(T& other) {
for (auto const& cur : other) {
insert(cur);
}
}
void insertAll(t_btree_5__3_0_1_2_4__8& other) {
ind_0.insertAll(other.ind_0);
}
bool contains(const t_tuple& t, context& h) const {
return ind_0.contains(t, h.hints_0);
}
bool contains(const t_tuple& t) const {
context h;
return contains(t, h);
}
std::size_t size() const {
return ind_0.size();
}
iterator find(const t_tuple& t, context& h) const {
return ind_0.find(t, h.hints_0);
}
iterator find(const t_tuple& t) const {
context h;
return find(t, h);
}
range<iterator> equalRange_0(const t_tuple& t, context& h) const {
return range<iterator>(ind_0.begin(),ind_0.end());
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t, context& h) const {
t_tuple low(t); t_tuple high(t);
low[0] = MIN_RAM_DOMAIN;
high[0] = MAX_RAM_DOMAIN;
low[1] = MIN_RAM_DOMAIN;
high[1] = MAX_RAM_DOMAIN;
low[2] = MIN_RAM_DOMAIN;
high[2] = MAX_RAM_DOMAIN;
low[4] = MIN_RAM_DOMAIN;
high[4] = MAX_RAM_DOMAIN;
return make_range(ind_0.lower_bound(low, h.hints_0), ind_0.upper_bound(high, h.hints_0));
}
range<t_ind_0::iterator> equalRange_8(const t_tuple& t) const {
context h;
return equalRange_8(t, h);
}
bool empty() const {
return ind_0.empty();
}
std::vector<range<iterator>> partition() const {
return ind_0.getChunks(400);
}
void purge() {
ind_0.clear();
}
iterator begin() const {
return ind_0.begin();
}
iterator end() const {
return ind_0.end();
}
void printHintStatistics(std::ostream& o, const std::string prefix) const {
const auto& stats_0 = ind_0.getHintStatistics();
o << prefix << "arity 5 direct b-tree index [3,0,1,2,4]: (hits/misses/total)\n";
o << prefix << "Insert: " << stats_0.inserts.getHits() << "/" << stats_0.inserts.getMisses() << "/" << stats_0.inserts.getAccesses() << "\n";
o << prefix << "Contains: " << stats_0.contains.getHits() << "/" << stats_0.contains.getMisses() << "/" << stats_0.contains.getAccesses() << "\n";
o << prefix << "Lower-bound: " << stats_0.lower_bound.getHits() << "/" << stats_0.lower_bound.getMisses() << "/" << stats_0.lower_bound.getAccesses() << "\n";
o << prefix << "Upper-bound: " << stats_0.upper_bound.getHits() << "/" << stats_0.upper_bound.getMisses() << "/" << stats_0.upper_bound.getAccesses() << "\n";
}
};

class Sf_null_pointer_checker_explicit_j16 : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(boolean)_",
	R"_(short)_",
	R"_(int)_",
	R"_(long)_",
	R"_(float)_",
	R"_(double)_",
	R"_(char)_",
	R"_(byte)_",
	R"_(<<null pseudo heap>>)_",
	R"_(Throw NullPointerException)_",
	R"_(java.lang.NullPointerException)_",
	R"_(Load Array Index)_",
	R"_(Store Array Index)_",
	R"_(Store Instance Field)_",
	R"_(Load Instance Field)_",
	R"_(Virtual Method Invocation)_",
	R"_(Special Method Invocation)_",
	R"_(Unary Operator)_",
	R"_(Binary Operator)_",
	R"_(Throw Null)_",
	R"_(Enter Monitor (Synchronized))_",
};// -- Table: ApplicationMethod
std::unique_ptr<t_brie_1__0__1> rel_1_ApplicationMethod = std::make_unique<t_brie_1__0__1>();
souffle::RelationWrapper<0,t_brie_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_1_ApplicationMethod;
// -- Table: AssignBinop
std::unique_ptr<t_btree_4__0_1_2_3> rel_2_AssignBinop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<1,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_2_AssignBinop;
// -- Table: AssignCastNull
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_3_AssignCastNull = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<2,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,true,false> wrapper_rel_3_AssignCastNull;
// -- Table: AssignOperFrom
std::unique_ptr<t_brie_2__0_1__3> rel_4_AssignOperFrom = std::make_unique<t_brie_2__0_1__3>();
souffle::RelationWrapper<3,t_brie_2__0_1__3,Tuple<RamDomain,2>,2,true,false> wrapper_rel_4_AssignOperFrom;
// -- Table: AssignUnop
std::unique_ptr<t_btree_4__0_1_2_3> rel_5_AssignUnop = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<4,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_5_AssignUnop;
// -- Table: CallGraphEdge
std::unique_ptr<t_btree_4__0_1_2_3> rel_6_CallGraphEdge = std::make_unique<t_btree_4__0_1_2_3>();
souffle::RelationWrapper<5,t_btree_4__0_1_2_3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_6_CallGraphEdge;
// -- Table: EnterMonitor
std::unique_ptr<t_btree_4__2_0_1_3__4> rel_7_EnterMonitor = std::make_unique<t_btree_4__2_0_1_3__4>();
souffle::RelationWrapper<6,t_btree_4__2_0_1_3__4,Tuple<RamDomain,4>,4,true,false> wrapper_rel_7_EnterMonitor;
// -- Table: InstructionLine
std::unique_ptr<t_btree_4__0_1_2_3__3> rel_8_InstructionLine = std::make_unique<t_btree_4__0_1_2_3__3>();
souffle::RelationWrapper<7,t_btree_4__0_1_2_3__3,Tuple<RamDomain,4>,4,true,false> wrapper_rel_8_InstructionLine;
// -- Table: LoadArrayIndex
std::unique_ptr<t_btree_5__3_4_0_1_2__8__24> rel_9_LoadArrayIndex = std::make_unique<t_btree_5__3_4_0_1_2__8__24>();
souffle::RelationWrapper<8,t_btree_5__3_4_0_1_2__8__24,Tuple<RamDomain,5>,5,true,false> wrapper_rel_9_LoadArrayIndex;
// -- Table: LoadInstanceField
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_10_LoadInstanceField = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<9,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_10_LoadInstanceField;
// -- Table: SpecialMethodInvocation
std::unique_ptr<t_btree_5__0_2_1_3_4__3_0_1_2_4__5__8> rel_11_SpecialMethodInvocation = std::make_unique<t_btree_5__0_2_1_3_4__3_0_1_2_4__5__8>();
souffle::RelationWrapper<10,t_btree_5__0_2_1_3_4__3_0_1_2_4__5__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_11_SpecialMethodInvocation;
// -- Table: StoreArrayIndex
std::unique_ptr<t_btree_5__3_4_0_1_2__8__24> rel_12_StoreArrayIndex = std::make_unique<t_btree_5__3_4_0_1_2__8__24>();
souffle::RelationWrapper<11,t_btree_5__3_4_0_1_2__8__24,Tuple<RamDomain,5>,5,true,false> wrapper_rel_12_StoreArrayIndex;
// -- Table: StoreInstanceField
std::unique_ptr<t_btree_6__3_0_1_2_4_5__8> rel_13_StoreInstanceField = std::make_unique<t_btree_6__3_0_1_2_4_5__8>();
souffle::RelationWrapper<12,t_btree_6__3_0_1_2_4_5__8,Tuple<RamDomain,6>,6,true,false> wrapper_rel_13_StoreInstanceField;
// -- Table: ThrowNull
std::unique_ptr<t_btree_3__0_1_2> rel_14_ThrowNull = std::make_unique<t_btree_3__0_1_2>();
souffle::RelationWrapper<13,t_btree_3__0_1_2,Tuple<RamDomain,3>,3,true,false> wrapper_rel_14_ThrowNull;
// -- Table: VarPointsTo
std::unique_ptr<t_btree_4__1_0_2_3__2> rel_15_VarPointsTo = std::make_unique<t_btree_4__1_0_2_3__2>();
souffle::RelationWrapper<14,t_btree_4__1_0_2_3__2,Tuple<RamDomain,4>,4,true,false> wrapper_rel_15_VarPointsTo;
// -- Table: VarPointsToNull
std::unique_ptr<t_brie_1__0> rel_16_VarPointsToNull = std::make_unique<t_brie_1__0>();
// -- Table: VirtualMethodInvocation
std::unique_ptr<t_btree_5__3_0_1_2_4__8> rel_17_VirtualMethodInvocation = std::make_unique<t_btree_5__3_0_1_2_4__8>();
souffle::RelationWrapper<15,t_btree_5__3_0_1_2_4__8,Tuple<RamDomain,5>,5,true,false> wrapper_rel_17_VirtualMethodInvocation;
// -- Table: NullAt
std::unique_ptr<t_btree_3__0_1_2> rel_18_NullAt = std::make_unique<t_btree_3__0_1_2>();
// -- Table: Reachable
std::unique_ptr<t_brie_1__0__1> rel_19_Reachable = std::make_unique<t_brie_1__0__1>();
souffle::RelationWrapper<16,t_brie_1__0__1,Tuple<RamDomain,1>,1,true,false> wrapper_rel_19_Reachable;
// -- Table: ReachableNullAt
std::unique_ptr<t_btree_3__0_1_2> rel_20_ReachableNullAt = std::make_unique<t_btree_3__0_1_2>();
// -- Table: ReachableNullAtLine
std::unique_ptr<t_btree_5__0_1_2_3_4> rel_21_ReachableNullAtLine = std::make_unique<t_btree_5__0_1_2_3_4>();
souffle::RelationWrapper<17,t_btree_5__0_1_2_3_4,Tuple<RamDomain,5>,5,false,true> wrapper_rel_21_ReachableNullAtLine;
public:
Sf_null_pointer_checker_explicit_j16() : 
wrapper_rel_1_ApplicationMethod(*rel_1_ApplicationMethod,symTable,"ApplicationMethod",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_2_AssignBinop(*rel_2_AssignBinop,symTable,"AssignBinop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_3_AssignCastNull(*rel_3_AssignCastNull,symTable,"AssignCastNull",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Type","s:Method"}},std::array<const char *,5>{{"ins","i","to","t","m"}}),

wrapper_rel_4_AssignOperFrom(*rel_4_AssignOperFrom,symTable,"AssignOperFrom",std::array<const char *,2>{{"s:Instruction","s:Var"}},std::array<const char *,2>{{"ins","from"}}),

wrapper_rel_5_AssignUnop(*rel_5_AssignUnop,symTable,"AssignUnop",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_6_CallGraphEdge(*rel_6_CallGraphEdge,symTable,"CallGraphEdge",std::array<const char *,4>{{"s:Context","s:Instruction","s:Context","s:Signature"}},std::array<const char *,4>{{"ctx","ins","hctx","sig"}}),

wrapper_rel_7_EnterMonitor(*rel_7_EnterMonitor,symTable,"EnterMonitor",std::array<const char *,4>{{"s:Instruction","i:Index","s:Var","s:Method"}},std::array<const char *,4>{{"ins","i","to","m"}}),

wrapper_rel_8_InstructionLine(*rel_8_InstructionLine,symTable,"InstructionLine",std::array<const char *,4>{{"s:Method","i:Index","i:LineNumber","s:File"}},std::array<const char *,4>{{"m","i","l","f"}}),

wrapper_rel_9_LoadArrayIndex(*rel_9_LoadArrayIndex,symTable,"LoadArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","to","base","m"}}),

wrapper_rel_10_LoadInstanceField(*rel_10_LoadInstanceField,symTable,"LoadInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Signature","s:Method"}},std::array<const char *,6>{{"ins","i","to","base","sig","m"}}),

wrapper_rel_11_SpecialMethodInvocation(*rel_11_SpecialMethodInvocation,symTable,"SpecialMethodInvocation",std::array<const char *,5>{{"s:symbol","i:Index","s:Signature","s:symbol","s:Method"}},std::array<const char *,5>{{"?instruction","i","sig","?base","m"}}),

wrapper_rel_12_StoreArrayIndex(*rel_12_StoreArrayIndex,symTable,"StoreArrayIndex",std::array<const char *,5>{{"s:Instruction","i:Index","s:Var","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","from","base","m"}}),

wrapper_rel_13_StoreInstanceField(*rel_13_StoreInstanceField,symTable,"StoreInstanceField",std::array<const char *,6>{{"s:Instruction","i:Index","s:Var","s:Var","s:Signature","s:Method"}},std::array<const char *,6>{{"ins","i","from","base","sig","m"}}),

wrapper_rel_14_ThrowNull(*rel_14_ThrowNull,symTable,"ThrowNull",std::array<const char *,3>{{"s:Instruction","i:Index","s:Method"}},std::array<const char *,3>{{"ins","i","m"}}),

wrapper_rel_15_VarPointsTo(*rel_15_VarPointsTo,symTable,"VarPointsTo",std::array<const char *,4>{{"s:HContext","s:Alloc","s:Context","s:Var"}},std::array<const char *,4>{{"hctx","a","ctx","v"}}),

wrapper_rel_17_VirtualMethodInvocation(*rel_17_VirtualMethodInvocation,symTable,"VirtualMethodInvocation",std::array<const char *,5>{{"s:Instruction","i:Index","s:Signature","s:Var","s:Method"}},std::array<const char *,5>{{"ins","i","sig","base","m"}}),

wrapper_rel_19_Reachable(*rel_19_Reachable,symTable,"Reachable",std::array<const char *,1>{{"s:Method"}},std::array<const char *,1>{{"m"}}),

wrapper_rel_21_ReachableNullAtLine(*rel_21_ReachableNullAtLine,symTable,"ReachableNullAtLine",std::array<const char *,5>{{"s:Method","i:Index","s:File","i:LineNumber","s:NPEType"}},std::array<const char *,5>{{"m","i","f","l","type"}}){
addRelation("ApplicationMethod",&wrapper_rel_1_ApplicationMethod,1,0);
addRelation("AssignBinop",&wrapper_rel_2_AssignBinop,1,0);
addRelation("AssignCastNull",&wrapper_rel_3_AssignCastNull,1,0);
addRelation("AssignOperFrom",&wrapper_rel_4_AssignOperFrom,1,0);
addRelation("AssignUnop",&wrapper_rel_5_AssignUnop,1,0);
addRelation("CallGraphEdge",&wrapper_rel_6_CallGraphEdge,1,0);
addRelation("EnterMonitor",&wrapper_rel_7_EnterMonitor,1,0);
addRelation("InstructionLine",&wrapper_rel_8_InstructionLine,1,0);
addRelation("LoadArrayIndex",&wrapper_rel_9_LoadArrayIndex,1,0);
addRelation("LoadInstanceField",&wrapper_rel_10_LoadInstanceField,1,0);
addRelation("SpecialMethodInvocation",&wrapper_rel_11_SpecialMethodInvocation,1,0);
addRelation("StoreArrayIndex",&wrapper_rel_12_StoreArrayIndex,1,0);
addRelation("StoreInstanceField",&wrapper_rel_13_StoreInstanceField,1,0);
addRelation("ThrowNull",&wrapper_rel_14_ThrowNull,1,0);
addRelation("VarPointsTo",&wrapper_rel_15_VarPointsTo,1,0);
addRelation("VirtualMethodInvocation",&wrapper_rel_17_VirtualMethodInvocation,1,0);
addRelation("Reachable",&wrapper_rel_19_Reachable,1,0);
addRelation("ReachableNullAtLine",&wrapper_rel_21_ReachableNullAtLine,0,1);
}
~Sf_null_pointer_checker_explicit_j16() {
}
private:
void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1, bool performIO = false) {
SignalHandler::instance()->set();
// -- initialize counter --
std::atomic<RamDomain> ctr(0);

std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(16);
#endif

// -- query evaluation --
/* BEGIN STRATUM 0 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_1_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_5_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_6_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 5 */
/* BEGIN STRATUM 6 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 6 */
/* BEGIN STRATUM 7 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 7 */
/* BEGIN STRATUM 8 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_9_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 8 */
/* BEGIN STRATUM 9 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 9 */
/* BEGIN STRATUM 10 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 10 */
/* BEGIN STRATUM 11 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_12_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 11 */
/* BEGIN STRATUM 12 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 12 */
/* BEGIN STRATUM 13 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_14_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 13 */
/* BEGIN STRATUM 14 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 14 */
/* BEGIN STRATUM 15 */
[&]() {
SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   VarPointsTo(_,"<<null pseudo heap>>",_,var).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [118:1-119:38])_");
if (!rel_15_VarPointsTo->empty()) [&](){
const Tuple<RamDomain,4> key({{0,RamDomain(8),0,0}});
auto range = rel_15_VarPointsTo->equalRange_2(key);
auto part = range.partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_15_VarPointsTo_op_ctxt,rel_15_VarPointsTo->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) { 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{(RamDomain)(env0[3])}});
rel_16_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(VarPointsToNull(var) :- 
   AssignCastNull(_,_,var,_,_).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [122:1-122:53])_");
if (!rel_3_AssignCastNull->empty()) [&](){
auto part = rel_3_AssignCastNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_3_AssignCastNull_op_ctxt,rel_3_AssignCastNull->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,1> tuple({{(RamDomain)(env0[2])}});
rel_16_VarPointsToNull->insert(tuple,READ_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_15_VarPointsTo->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_AssignCastNull->purge();
}();
/* END STRATUM 15 */
/* BEGIN STRATUM 16 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 16 */
/* BEGIN STRATUM 17 */
[&]() {
SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw NullPointerException") :- 
   CallGraphEdge(_,a,_,b),
   SpecialMethodInvocation(a,index,b,_,meth),
   "java.lang.NullPointerException" contains a.
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [126:1-129:47])_");
if (!rel_6_CallGraphEdge->empty()&&!rel_11_SpecialMethodInvocation->empty()) [&](){
auto part = rel_6_CallGraphEdge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_6_CallGraphEdge_op_ctxt,rel_6_CallGraphEdge->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_SpecialMethodInvocation_op_ctxt,rel_11_SpecialMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
if( (symTable.resolve(env0[1]).find(symTable.resolve(RamDomain(10))) != std::string::npos)) {
const Tuple<RamDomain,5> key({{env0[1],0,env0[3],0,0}});
auto range = rel_11_SpecialMethodInvocation->equalRange_5(key,READ_OP_CONTEXT(rel_11_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[4]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(9))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Array Index") :- 
   VarPointsToNull(var),
   LoadArrayIndex(_,index,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [132:1-134:40])_");
if (!rel_9_LoadArrayIndex->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_LoadArrayIndex_op_ctxt,rel_9_LoadArrayIndex->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_9_LoadArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_9_LoadArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[4]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(11))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Array Index") :- 
   VarPointsToNull(var),
   StoreArrayIndex(_,index,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [141:1-143:41])_");
if (!rel_12_StoreArrayIndex->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_12_StoreArrayIndex_op_ctxt,rel_12_StoreArrayIndex->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_12_StoreArrayIndex->equalRange_8(key,READ_OP_CONTEXT(rel_12_StoreArrayIndex_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[4]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(12))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Store Instance Field") :- 
   VarPointsToNull(var),
   StoreInstanceField(_,index,_,var,_,meth),
   !StoreArrayIndex(_,_,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [150:1-153:38])_");
if (!rel_13_StoreInstanceField->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_12_StoreArrayIndex_op_ctxt,rel_12_StoreArrayIndex->createContext());
CREATE_OP_CONTEXT(rel_13_StoreInstanceField_op_ctxt,rel_13_StoreInstanceField->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_13_StoreInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_13_StoreInstanceField_op_ctxt));
for(const auto& env1 : range) {
if( rel_12_StoreArrayIndex->equalRange_24(Tuple<RamDomain,5>({{0,0,0,env0[0],env1[5]}}),READ_OP_CONTEXT(rel_12_StoreArrayIndex_op_ctxt)).empty()) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[5]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(13))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Load Instance Field") :- 
   VarPointsToNull(var),
   LoadInstanceField(_,index,_,var,_,meth),
   !LoadArrayIndex(_,_,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [161:1-164:37])_");
if (!rel_10_LoadInstanceField->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_LoadArrayIndex_op_ctxt,rel_9_LoadArrayIndex->createContext());
CREATE_OP_CONTEXT(rel_10_LoadInstanceField_op_ctxt,rel_10_LoadInstanceField->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,6> key({{0,0,0,env0[0],0,0}});
auto range = rel_10_LoadInstanceField->equalRange_8(key,READ_OP_CONTEXT(rel_10_LoadInstanceField_op_ctxt));
for(const auto& env1 : range) {
if( rel_9_LoadArrayIndex->equalRange_24(Tuple<RamDomain,5>({{0,0,0,env0[0],env1[5]}}),READ_OP_CONTEXT(rel_9_LoadArrayIndex_op_ctxt)).empty()) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[5]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(14))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Virtual Method Invocation") :- 
   VarPointsToNull(var),
   VirtualMethodInvocation(_,index,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [173:1-175:49])_");
if (!rel_16_VarPointsToNull->empty()&&!rel_17_VirtualMethodInvocation->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
CREATE_OP_CONTEXT(rel_17_VirtualMethodInvocation_op_ctxt,rel_17_VirtualMethodInvocation->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_17_VirtualMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_17_VirtualMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[4]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(15))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Special Method Invocation") :- 
   VarPointsToNull(var),
   SpecialMethodInvocation(_,index,_,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [181:1-183:49])_");
if (!rel_11_SpecialMethodInvocation->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_11_SpecialMethodInvocation_op_ctxt,rel_11_SpecialMethodInvocation->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,5> key({{0,0,0,env0[0],0}});
auto range = rel_11_SpecialMethodInvocation->equalRange_8(key,READ_OP_CONTEXT(rel_11_SpecialMethodInvocation_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[4]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(16))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Unary Operator") :- 
   VarPointsToNull(var),
   AssignUnop(ins,index,_,meth),
   AssignOperFrom(ins,var).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [190:1-193:26])_");
if (!rel_4_AssignOperFrom->empty()&&!rel_5_AssignUnop->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_4_AssignOperFrom_op_ctxt,rel_4_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_5_AssignUnop_op_ctxt,rel_5_AssignUnop->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_5_AssignUnop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_4_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_4_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[3]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(17))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Binary Operator") :- 
   VarPointsToNull(var),
   AssignBinop(ins,index,_,meth),
   AssignOperFrom(ins,var).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [202:1-205:26])_");
if (!rel_2_AssignBinop->empty()&&!rel_4_AssignOperFrom->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_2_AssignBinop_op_ctxt,rel_2_AssignBinop->createContext());
CREATE_OP_CONTEXT(rel_4_AssignOperFrom_op_ctxt,rel_4_AssignOperFrom->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
for(const auto& env1 : *rel_2_AssignBinop) {
const Tuple<RamDomain,2> key({{env1[0],env0[0]}});
auto range = rel_4_AssignOperFrom->equalRange_3(key,READ_OP_CONTEXT(rel_4_AssignOperFrom_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[3]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(18))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Throw Null") :- 
   ThrowNull(_,index,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [214:1-215:27])_");
if (!rel_14_ThrowNull->empty()) [&](){
auto part = rel_14_ThrowNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_14_ThrowNull_op_ctxt,rel_14_ThrowNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env0[2]),(RamDomain)(env0[1]),(RamDomain)(RamDomain(19))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();SignalHandler::instance()->setMsg(R"_(NullAt(meth,index,"Enter Monitor (Synchronized)") :- 
   VarPointsToNull(var),
   EnterMonitor(_,index,var,meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [217:1-219:35])_");
if (!rel_7_EnterMonitor->empty()&&!rel_16_VarPointsToNull->empty()) [&](){
auto part = rel_16_VarPointsToNull->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_7_EnterMonitor_op_ctxt,rel_7_EnterMonitor->createContext());
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_16_VarPointsToNull_op_ctxt,rel_16_VarPointsToNull->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{0,0,env0[0],0}});
auto range = rel_7_EnterMonitor->equalRange_4(key,READ_OP_CONTEXT(rel_7_EnterMonitor_op_ctxt));
for(const auto& env1 : range) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env1[3]),(RamDomain)(env1[1]),(RamDomain)(RamDomain(20))}});
rel_18_NullAt->insert(tuple,READ_OP_CONTEXT(rel_18_NullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_6_CallGraphEdge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_11_SpecialMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_9_LoadArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_12_StoreArrayIndex->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_13_StoreInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_10_LoadInstanceField->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_17_VirtualMethodInvocation->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_14_ThrowNull->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_5_AssignUnop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_AssignBinop->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_AssignOperFrom->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_EnterMonitor->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_16_VarPointsToNull->purge();
}();
/* END STRATUM 17 */
/* BEGIN STRATUM 18 */
[&]() {
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/Reachable.csv"},{"name","Reachable"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_19_Reachable);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
}();
/* END STRATUM 18 */
/* BEGIN STRATUM 19 */
[&]() {
SignalHandler::instance()->setMsg(R"_(ReachableNullAt(meth,index,type) :- 
   NullAt(meth,index,type),
   Reachable(meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [228:1-228:82])_");
if (!rel_18_NullAt->empty()&&!rel_19_Reachable->empty()) [&](){
auto part = rel_18_NullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_18_NullAt_op_ctxt,rel_18_NullAt->createContext());
CREATE_OP_CONTEXT(rel_19_Reachable_op_ctxt,rel_19_Reachable->createContext());
CREATE_OP_CONTEXT(rel_20_ReachableNullAt_op_ctxt,rel_20_ReachableNullAt->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_19_Reachable->equalRange_1(key,READ_OP_CONTEXT(rel_19_Reachable_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,3> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[1]),(RamDomain)(env0[2])}});
rel_20_ReachableNullAt->insert(tuple,READ_OP_CONTEXT(rel_20_ReachableNullAt_op_ctxt));
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (!isHintsProfilingEnabled() && (performIO || 0)) rel_19_Reachable->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_18_NullAt->purge();
}();
/* END STRATUM 19 */
/* BEGIN STRATUM 20 */
[&]() {
SignalHandler::instance()->setMsg(R"_(ReachableNullAtLine(meth,index,file,line,type) :- 
   ReachableNullAt(meth,index,type),
   InstructionLine(meth,index,line,file),
   ApplicationMethod(meth).
in file /home/gfun5990/info3600-bugchecker/digger/logic/doop_logic/null-pointer-checker-explicit/null-pointer-checker-explicit.dl [230:1-233:25])_");
if (!rel_1_ApplicationMethod->empty()&&!rel_8_InstructionLine->empty()&&!rel_20_ReachableNullAt->empty()) [&](){
auto part = rel_20_ReachableNullAt->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_1_ApplicationMethod_op_ctxt,rel_1_ApplicationMethod->createContext());
CREATE_OP_CONTEXT(rel_8_InstructionLine_op_ctxt,rel_8_InstructionLine->createContext());
CREATE_OP_CONTEXT(rel_20_ReachableNullAt_op_ctxt,rel_20_ReachableNullAt->createContext());
CREATE_OP_CONTEXT(rel_21_ReachableNullAtLine_op_ctxt,rel_21_ReachableNullAtLine->createContext());
pfor(auto it = part.begin(); it<part.end();++it){
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,4> key({{env0[0],env0[1],0,0}});
auto range = rel_8_InstructionLine->equalRange_3(key,READ_OP_CONTEXT(rel_8_InstructionLine_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,1> key({{env0[0]}});
auto range = rel_1_ApplicationMethod->equalRange_1(key,READ_OP_CONTEXT(rel_1_ApplicationMethod_op_ctxt));
if(!range.empty()) {
Tuple<RamDomain,5> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[1]),(RamDomain)(env1[3]),(RamDomain)(env1[2]),(RamDomain)(env0[2])}});
rel_21_ReachableNullAtLine->insert(tuple,READ_OP_CONTEXT(rel_21_ReachableNullAtLine_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
}
PARALLEL_END;
}
();if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","stdout"},{"attributeNames","m\ti\tf\tl\ttype"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_InstructionLine->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_ApplicationMethod->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_20_ReachableNullAt->purge();
}();
/* END STRATUM 20 */

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_ApplicationMethod:\n";
rel_1_ApplicationMethod->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_AssignBinop:\n";
rel_2_AssignBinop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_AssignCastNull:\n";
rel_3_AssignCastNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_AssignOperFrom:\n";
rel_4_AssignOperFrom->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_AssignUnop:\n";
rel_5_AssignUnop->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_CallGraphEdge:\n";
rel_6_CallGraphEdge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_EnterMonitor:\n";
rel_7_EnterMonitor->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_InstructionLine:\n";
rel_8_InstructionLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_LoadArrayIndex:\n";
rel_9_LoadArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_LoadInstanceField:\n";
rel_10_LoadInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_11_SpecialMethodInvocation:\n";
rel_11_SpecialMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_12_StoreArrayIndex:\n";
rel_12_StoreArrayIndex->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_13_StoreInstanceField:\n";
rel_13_StoreInstanceField->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_14_ThrowNull:\n";
rel_14_ThrowNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_15_VarPointsTo:\n";
rel_15_VarPointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_16_VarPointsToNull:\n";
rel_16_VarPointsToNull->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_17_VirtualMethodInvocation:\n";
rel_17_VirtualMethodInvocation->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_18_NullAt:\n";
rel_18_NullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_19_Reachable:\n";
rel_19_Reachable->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_20_ReachableNullAt:\n";
rel_20_ReachableNullAt->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_21_ReachableNullAtLine:\n";
rel_21_ReachableNullAtLine->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction(".", ".", stratumIndex, false); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction(inputDirectory, outputDirectory, stratumIndex, true);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","stdout"},{"attributeNames","m\ti\tf\tl\ttype"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","m\ti\tf\tl\ttype"},{"delimiter","\t"},{"filename","./NullPointerExceptions.csv"},{"name","ReachableNullAtLine"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./database/ApplicationMethod.csv"},{"name","ApplicationMethod"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_1_ApplicationMethod);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignBinop.facts"},{"name","AssignBinop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_AssignBinop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignCastNull.facts"},{"name","AssignCastNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_AssignCastNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignOperFrom.facts"},{"name","AssignOperFrom"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_4_AssignOperFrom);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/AssignUnop.facts"},{"name","AssignUnop"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_5_AssignUnop);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/CallGraphEdge.csv"},{"name","CallGraphEdge"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_6_CallGraphEdge);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/EnterMonitor.facts"},{"name","EnterMonitor"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_EnterMonitor);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/Instruction-Line.facts"},{"name","InstructionLine"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_8_InstructionLine);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadArrayIndex.facts"},{"name","LoadArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_9_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/LoadInstanceField.facts"},{"name","LoadInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_10_LoadInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/SpecialMethodInvocation.facts"},{"name","SpecialMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_11_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreArrayIndex.facts"},{"name","StoreArrayIndex"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_12_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/StoreInstanceField.facts"},{"name","StoreInstanceField"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_13_StoreInstanceField);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/ThrowNull.facts"},{"name","ThrowNull"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->readAll(*rel_14_ThrowNull);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/VarPointsTo.csv"},{"name","VarPointsTo"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_15_VarPointsTo);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"delimiter","\t"},{"filename","./facts/VirtualMethodInvocation.facts"},{"name","VirtualMethodInvocation"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_17_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./database/Reachable.csv"},{"name","Reachable"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1}), symTable, ioDirectives, 0)->readAll(*rel_19_Reachable);
} catch (std::exception& e) {std::cerr << "Error loading data: " << e.what() << '\n';}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_ApplicationMethod");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_1_ApplicationMethod);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_AssignBinop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_AssignBinop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_AssignCastNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_AssignCastNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_AssignOperFrom");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_AssignOperFrom);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_5_AssignUnop");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_5_AssignUnop);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_6_CallGraphEdge");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_6_CallGraphEdge);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_7_EnterMonitor");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_7_EnterMonitor);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_InstructionLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_InstructionLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_9_LoadArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_9_LoadArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_10_LoadInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_10_LoadInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_11_SpecialMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_11_SpecialMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_12_StoreArrayIndex");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_12_StoreArrayIndex);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_13_StoreInstanceField");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_13_StoreInstanceField);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_14_ThrowNull");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_14_ThrowNull);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_15_VarPointsTo");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_15_VarPointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_17_VirtualMethodInvocation");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_17_VirtualMethodInvocation);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_19_Reachable");
IOSystem::getInstance().getWriter(SymbolMask({1}), symTable, ioDirectives, 0)->writeAll(*rel_19_Reachable);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_21_ReachableNullAtLine");
IOSystem::getInstance().getWriter(SymbolMask({1, 0, 1, 0, 1}), symTable, ioDirectives, 0)->writeAll(*rel_21_ReachableNullAtLine);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_null_pointer_checker_explicit_j16(){return new Sf_null_pointer_checker_explicit_j16;}
SymbolTable *getST_null_pointer_checker_explicit_j16(SouffleProgram *p){return &reinterpret_cast<Sf_null_pointer_checker_explicit_j16*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_null_pointer_checker_explicit_j16: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_null_pointer_checker_explicit_j16();
};
public:
factory_Sf_null_pointer_checker_explicit_j16() : ProgramFactory("null_pointer_checker_explicit_j16"){}
};
static factory_Sf_null_pointer_checker_explicit_j16 __factory_Sf_null_pointer_checker_explicit_j16_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(null-pointer-checker-explicit/null-pointer-checker-explicit.dl)",
R"(.)",
R"(.)",
false,
R"()",
16,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf_null_pointer_checker_explicit_j16 obj;
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
