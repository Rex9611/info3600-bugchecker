import sys
import json
import linecache
import os.path

# Process the raw souffle output to a set of line numbers that may contain NPE
def process(raw):
	arr = []
	save = False
	i = 0
	with open(raw, "r") as file:
		for line in file:
			if i == 2:
				break
			if save == True:
				if line.startswith('='): i += 1
				else: arr.append(line.split("\t")[3])
			if line == "ReachableNullAtLine\n":
				save = True
	return arr

# Print the detected Exception lines and corresponding code
def print_result(result, java_file):
	line_count = sum(1 for line in open(java_file))
	if len(result) == 0:
		print("No Null Pointer Expcetion is found :) ")

	for line in result:
		if 0 < int(line) <= line_count:
			print("Find Null Pointer Exception at line " + line)
			print("\033[90m" + linecache.getline(java_file, int(line)) + "\033[0;m")
		else:
			print("The line number " + line + " is out of bound :(")

# Compare the anlysis result with the desired result in out file
def compare(result, expected):
	for line in expected:
		if str(line) not in result:
			print("\033[1;31mExcpetion on line " + str(line) + " is not detected :(\033[1;m")
			return False
		else:
			result.remove(str(line))
	for r in result:
		print("\033[1;31mExcpetion on line " + r + " is not expected :(\033[1;m")
	return len(result) == 0


#this program takes three arguments: 
	# 1.raw result file path from souffle. 
	# 2. directory of java files and expected results json file 
	# 3. target java file name
def main():
	raw_result=str(sys.argv[1])
	result = process(raw_result)
	java_file = sys.argv[2] + '/' + sys.argv[3] + '.java'
	print_result(result, java_file)

	expected_file = sys.argv[2] + '/results.json'
	expected = None
	expected_exists = os.path.isfile(expected_file)
	if expected_exists:
		with open(expected_file) as f: 
			expected_json = json.load(f)
			expected = expected_json[sys.argv[3]] if sys.argv[3] in expected_json else []
		pas = "\033[1;32mPass\033[1;m" if compare(result, expected) else "\033[1;31mFail\033[1;m"
		print("\033[1m"+ pas + "\033[0m")
	else:
		print("\033[1;31mExpected Result File Does Not Exists.\033[1;m")

if __name__ == "__main__": main()
