
#include "souffle/CompiledSouffle.h"

namespace souffle {
using namespace ram;
class Sf_example : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
private:
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
private:
static inline RamDomain wrapper_tonumber(const std::string& str) {
   RamDomain result=0; 
   try { result = stord(str); } catch(...) { 
     std::cerr << "error: wrong string provided by to_number(\"";
     std::cerr << str << "\") functor.\n";
     raise(SIGFPE);
   } return result;
}
public:
// -- initialize symbol table --
SymbolTable symTable
{
	R"_(0)_",
	R"_(1)_",
	R"_(2)_",
	R"_(3)_",
	R"_(4)_",
};// -- Table: edge
ram::Relation<Auto,2>* rel_1_edge;
// -- Table: path
ram::Relation<Auto,2, ram::index<0,1>>* rel_2_path;
souffle::RelationWrapper<0,ram::Relation<Auto,2, ram::index<0,1>>,Tuple<RamDomain,2>,2,false,true> wrapper_rel_2_path;
// -- Table: @delta_path
ram::Relation<Auto,2, ram::index<0>>* rel_3_delta_path;
// -- Table: @new_path
ram::Relation<Auto,2, ram::index<0>>* rel_4_new_path;
public:
Sf_example() : rel_1_edge(new ram::Relation<Auto,2>()),
rel_2_path(new ram::Relation<Auto,2, ram::index<0,1>>()),
wrapper_rel_2_path(*rel_2_path,symTable,"path",std::array<const char *,2>{{"s:Node","s:Node"}},std::array<const char *,2>{{"n","m"}}),
rel_3_delta_path(new ram::Relation<Auto,2, ram::index<0>>()),
rel_4_new_path(new ram::Relation<Auto,2, ram::index<0>>()){
addRelation("path",&wrapper_rel_2_path,0,1);
}
~Sf_example() {
delete rel_1_edge;
delete rel_2_path;
delete rel_3_delta_path;
delete rel_4_new_path;
}
private:
template <bool performIO> void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) {
SignalHandler::instance()->set();
// -- initialize counter --
std::atomic<RamDomain> ctr(0);

std::atomic<size_t> iter(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(1);
#endif

// -- query evaluation --
/* BEGIN STRATUM 0 */
{
SignalHandler::instance()->setMsg(R"_(edge("0","1").
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [6:1-6:16])_");
rel_1_edge->insert(RamDomain(0),RamDomain(1));
SignalHandler::instance()->setMsg(R"_(edge("1","2").
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [7:1-7:16])_");
rel_1_edge->insert(RamDomain(1),RamDomain(2));
SignalHandler::instance()->setMsg(R"_(edge("2","3").
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [8:1-8:16])_");
rel_1_edge->insert(RamDomain(2),RamDomain(3));
SignalHandler::instance()->setMsg(R"_(edge("3","4").
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [9:1-9:16])_");
rel_1_edge->insert(RamDomain(3),RamDomain(4));
}
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
{
SignalHandler::instance()->setMsg(R"_(path(x,y) :- 
   edge(x,y).
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [15:1-15:26])_");
if (!rel_1_edge->empty()) {
auto part = rel_1_edge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_1_edge_op_ctxt,rel_1_edge->createContext());
CREATE_OP_CONTEXT(rel_2_path_op_ctxt,rel_2_path->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[1])}});
rel_2_path->insert(tuple,READ_OP_CONTEXT(rel_2_path_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
rel_3_delta_path->insertAll(*rel_2_path);
iter = 0;
for(;;) {
SignalHandler::instance()->setMsg(R"_(path(x,y) :- 
   edge(x,z),
   path(z,y).
in file /mnt/c/Shared/INFO3600/info3600-bugchecker/Datalog/Gavin/path.d1 [16:1-16:38])_");
if (!rel_3_delta_path->empty()&&!rel_1_edge->empty()) {
auto part = rel_1_edge->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_3_delta_path_op_ctxt,rel_3_delta_path->createContext());
CREATE_OP_CONTEXT(rel_4_new_path_op_ctxt,rel_4_new_path->createContext());
CREATE_OP_CONTEXT(rel_1_edge_op_ctxt,rel_1_edge->createContext());
CREATE_OP_CONTEXT(rel_2_path_op_ctxt,rel_2_path->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_3_delta_path->equalRange<0>(key,READ_OP_CONTEXT(rel_3_delta_path_op_ctxt));
for(const auto& env1 : range) {
if( !rel_2_path->contains(Tuple<RamDomain,2>({{env0[0],env1[1]}}),READ_OP_CONTEXT(rel_2_path_op_ctxt))) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env1[1])}});
rel_4_new_path->insert(tuple,READ_OP_CONTEXT(rel_4_new_path_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
if(rel_4_new_path->empty()) break;
rel_2_path->insertAll(*rel_4_new_path);
{
auto rel_0 = rel_3_delta_path;
rel_3_delta_path = rel_4_new_path;
rel_4_new_path = rel_0;
}
rel_4_new_path->purge();
iter++;
}
iter = 0;
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_3_delta_path->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_4_new_path->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","n\tm"},{"filename","./path.csv"},{"name","path"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_path);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_edge->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_path->purge();
}
/* END STRATUM 1 */

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_edge:\n";
rel_1_edge->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_path:\n";
rel_2_path->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_delta_path:\n";
rel_3_delta_path->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_new_path:\n";
rel_4_new_path->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction<false>(".", ".", stratumIndex); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction<true>(inputDirectory, outputDirectory, stratumIndex);
}
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","n\tm"},{"filename","./path.csv"},{"name","path"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_path);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void loadAll(std::string inputDirectory = ".") override {
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_path");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_path);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_example(){return new Sf_example;}
SymbolTable *getST_example(SouffleProgram *p){return &reinterpret_cast<Sf_example*>(p)->symTable;}

#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_example: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_example();
};
public:
factory_Sf_example() : ProgramFactory("example"){}
};
static factory_Sf_example __factory_Sf_example_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(path.d1)",
R"(.)",
R"(.)",
false,
R"()",
1,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);

#endif
souffle::Sf_example obj;
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}

#endif
