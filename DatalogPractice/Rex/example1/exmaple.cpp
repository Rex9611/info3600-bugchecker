#include "souffle/CompiledSouffle.h"

namespace souffle {
using namespace ram;
class Sf_exmaple : public SouffleProgram {
private:
static inline bool regex_wrapper(const std::string& pattern, const std::string& text) {
   bool result = false; 
   try { result = std::regex_match(text, std::regex(pattern)); } catch(...) { 
     std::cerr << "warning: wrong pattern provided for match(\"" << pattern << "\",\"" << text << "\").\n";
}
   return result;
}
static inline std::string substr_wrapper(const std::string& str, size_t idx, size_t len) {
   std::string result; 
   try { result = str.substr(idx,len); } catch(...) { 
     std::cerr << "warning: wrong index position provided by substr(\"";
     std::cerr << str << "\"," << (int32_t)idx << "," << (int32_t)len << ") functor.\n";
   } return result;
}
public:
// -- initialize symbol table --
SymbolTable symTable
;// -- Table: assign
ram::Relation<Auto,2>* rel_1_assign;
souffle::RelationWrapper<0,ram::Relation<Auto,2>,Tuple<RamDomain,2>,2,true,false> wrapper_rel_1_assign;
// -- Table: load
ram::Relation<Auto,3>* rel_2_load;
souffle::RelationWrapper<1,ram::Relation<Auto,3>,Tuple<RamDomain,3>,3,true,false> wrapper_rel_2_load;
// -- Table: store
ram::Relation<Auto,3, ram::index<0,1>>* rel_3_store;
souffle::RelationWrapper<2,ram::Relation<Auto,3, ram::index<0,1>>,Tuple<RamDomain,3>,3,true,false> wrapper_rel_3_store;
// -- Table: alias
ram::Relation<Auto,2, ram::index<0,1>>* rel_4_alias;
souffle::RelationWrapper<3,ram::Relation<Auto,2, ram::index<0,1>>,Tuple<RamDomain,2>,2,false,true> wrapper_rel_4_alias;
// -- Table: @delta_alias
ram::Relation<Auto,2, ram::index<0>>* rel_5_delta_alias;
// -- Table: @new_alias
ram::Relation<Auto,2, ram::index<0>>* rel_6_new_alias;
// -- Table: new
ram::Relation<Auto,2>* rel_7_new;
souffle::RelationWrapper<4,ram::Relation<Auto,2>,Tuple<RamDomain,2>,2,true,false> wrapper_rel_7_new;
// -- Table: pointsTo
ram::Relation<Auto,2, ram::index<0,1>>* rel_8_pointsTo;
souffle::RelationWrapper<5,ram::Relation<Auto,2, ram::index<0,1>>,Tuple<RamDomain,2>,2,false,true> wrapper_rel_8_pointsTo;
// -- Table: @delta_pointsTo
ram::Relation<Auto,2, ram::index<0>>* rel_9_delta_pointsTo;
// -- Table: @new_pointsTo
ram::Relation<Auto,2, ram::index<0>>* rel_10_new_pointsTo;
public:
Sf_exmaple() : rel_1_assign(new ram::Relation<Auto,2>()),
wrapper_rel_1_assign(*rel_1_assign,symTable,"assign",std::array<const char *,2>{{"s:var","s:var"}},std::array<const char *,2>{{"a","b"}}),
rel_2_load(new ram::Relation<Auto,3>()),
wrapper_rel_2_load(*rel_2_load,symTable,"load",std::array<const char *,3>{{"s:var","s:var","s:fld"}},std::array<const char *,3>{{"a","b","f"}}),
rel_3_store(new ram::Relation<Auto,3, ram::index<0,1>>()),
wrapper_rel_3_store(*rel_3_store,symTable,"store",std::array<const char *,3>{{"s:var","s:fld","s:var"}},std::array<const char *,3>{{"a","b","v"}}),
rel_4_alias(new ram::Relation<Auto,2, ram::index<0,1>>()),
wrapper_rel_4_alias(*rel_4_alias,symTable,"alias",std::array<const char *,2>{{"s:var","s:var"}},std::array<const char *,2>{{"a","b"}}),
rel_5_delta_alias(new ram::Relation<Auto,2, ram::index<0>>()),
rel_6_new_alias(new ram::Relation<Auto,2, ram::index<0>>()),
rel_7_new(new ram::Relation<Auto,2>()),
wrapper_rel_7_new(*rel_7_new,symTable,"new",std::array<const char *,2>{{"s:var","s:obj"}},std::array<const char *,2>{{"v","o"}}),
rel_8_pointsTo(new ram::Relation<Auto,2, ram::index<0,1>>()),
wrapper_rel_8_pointsTo(*rel_8_pointsTo,symTable,"pointsTo",std::array<const char *,2>{{"s:var","s:obj"}},std::array<const char *,2>{{"a","o"}}),
rel_9_delta_pointsTo(new ram::Relation<Auto,2, ram::index<0>>()),
rel_10_new_pointsTo(new ram::Relation<Auto,2, ram::index<0>>()){
addRelation("assign",&wrapper_rel_1_assign,1,0);
addRelation("load",&wrapper_rel_2_load,1,0);
addRelation("store",&wrapper_rel_3_store,1,0);
addRelation("alias",&wrapper_rel_4_alias,0,1);
addRelation("new",&wrapper_rel_7_new,1,0);
addRelation("pointsTo",&wrapper_rel_8_pointsTo,0,1);
}
~Sf_exmaple() {
delete rel_1_assign;
delete rel_2_load;
delete rel_3_store;
delete rel_4_alias;
delete rel_5_delta_alias;
delete rel_6_new_alias;
delete rel_7_new;
delete rel_8_pointsTo;
delete rel_9_delta_pointsTo;
delete rel_10_new_pointsTo;
}
private:
template <bool performIO> void runFunction(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) {
SignalHandler::instance()->set();
// -- initialize counter --
std::atomic<RamDomain> ctr(0);

#if defined(__EMBEDDED_SOUFFLE__) && defined(_OPENMP)
omp_set_num_threads(1);
#endif

// -- query evaluation --
/* BEGIN STRATUM 0 */
{
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/assign.facts"},{"name","assign"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_assign);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
/* END STRATUM 0 */
/* BEGIN STRATUM 1 */
{
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/load.facts"},{"name","load"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_load);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
/* END STRATUM 1 */
/* BEGIN STRATUM 2 */
{
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/store.facts"},{"name","store"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_store);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
/* END STRATUM 2 */
/* BEGIN STRATUM 3 */
{
SignalHandler::instance()->setMsg(R"_(alias(X,X) :- 
   assign(X,_).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [27:1-27:27])_");
if (!rel_1_assign->empty()) {
auto part = rel_1_assign->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_4_alias_op_ctxt,rel_4_alias->createContext());
CREATE_OP_CONTEXT(rel_1_assign_op_ctxt,rel_1_assign->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[0])}});
rel_4_alias->insert(tuple,READ_OP_CONTEXT(rel_4_alias_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
SignalHandler::instance()->setMsg(R"_(alias(X,X) :- 
   assign(_,X).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [28:1-28:27])_");
if (!rel_1_assign->empty()) {
auto part = rel_1_assign->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_4_alias_op_ctxt,rel_4_alias->createContext());
CREATE_OP_CONTEXT(rel_1_assign_op_ctxt,rel_1_assign->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[1]),(RamDomain)(env0[1])}});
rel_4_alias->insert(tuple,READ_OP_CONTEXT(rel_4_alias_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
SignalHandler::instance()->setMsg(R"_(alias(X,Y) :- 
   assign(X,Y).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [29:1-29:27])_");
if (!rel_1_assign->empty()) {
auto part = rel_1_assign->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_4_alias_op_ctxt,rel_4_alias->createContext());
CREATE_OP_CONTEXT(rel_1_assign_op_ctxt,rel_1_assign->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[1])}});
rel_4_alias->insert(tuple,READ_OP_CONTEXT(rel_4_alias_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
rel_5_delta_alias->insertAll(*rel_4_alias);
for(;;) {
SignalHandler::instance()->setMsg(R"_(alias(X,Y) :- 
   load(X,A,F),
   alias(A,B),
   store(B,F,Y).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [30:1-30:53])_");
if (!rel_5_delta_alias->empty()&&!rel_2_load->empty()&&!rel_3_store->empty()) {
auto part = rel_2_load->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_5_delta_alias_op_ctxt,rel_5_delta_alias->createContext());
CREATE_OP_CONTEXT(rel_6_new_alias_op_ctxt,rel_6_new_alias->createContext());
CREATE_OP_CONTEXT(rel_4_alias_op_ctxt,rel_4_alias->createContext());
CREATE_OP_CONTEXT(rel_2_load_op_ctxt,rel_2_load->createContext());
CREATE_OP_CONTEXT(rel_3_store_op_ctxt,rel_3_store->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_5_delta_alias->equalRange<0>(key,READ_OP_CONTEXT(rel_5_delta_alias_op_ctxt));
for(const auto& env1 : range) {
const Tuple<RamDomain,3> key({{env1[1],env0[2],0}});
auto range = rel_3_store->equalRange<0,1>(key,READ_OP_CONTEXT(rel_3_store_op_ctxt));
for(const auto& env2 : range) {
if( !rel_4_alias->contains(Tuple<RamDomain,2>({{env0[0],env2[2]}}),READ_OP_CONTEXT(rel_4_alias_op_ctxt))) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env2[2])}});
rel_6_new_alias->insert(tuple,READ_OP_CONTEXT(rel_6_new_alias_op_ctxt));
}
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
if(rel_6_new_alias->empty()) break;
rel_4_alias->insertAll(*rel_6_new_alias);
{
auto rel_0 = rel_5_delta_alias;
rel_5_delta_alias = rel_6_new_alias;
rel_6_new_alias = rel_0;
}
rel_6_new_alias->purge();
}
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_5_delta_alias->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_6_new_alias->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","a\tb"},{"filename","./output/alias.csv"},{"name","alias"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_alias);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_1_assign->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_2_load->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_3_store->purge();
}
/* END STRATUM 3 */
/* BEGIN STRATUM 4 */
{
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/new.facts"},{"name","new"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_new);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
}
/* END STRATUM 4 */
/* BEGIN STRATUM 5 */
{
SignalHandler::instance()->setMsg(R"_(pointsTo(X,Y) :- 
   new(X,Y).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [34:1-34:27])_");
if (!rel_7_new->empty()) {
auto part = rel_7_new->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_7_new_op_ctxt,rel_7_new->createContext());
CREATE_OP_CONTEXT(rel_8_pointsTo_op_ctxt,rel_8_pointsTo->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env0[1])}});
rel_8_pointsTo->insert(tuple,READ_OP_CONTEXT(rel_8_pointsTo_op_ctxt));
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
rel_9_delta_pointsTo->insertAll(*rel_8_pointsTo);
for(;;) {
SignalHandler::instance()->setMsg(R"_(pointsTo(X,Y) :- 
   alias(X,Z),
   pointsTo(Z,Y).
in file /Users/Admin/Projects/info3600-bugchecker/Datalog/Rex/exmaple.dl [35:1-35:44])_");
if (!rel_9_delta_pointsTo->empty()&&!rel_4_alias->empty()) {
auto part = rel_4_alias->partition();
PARALLEL_START;
CREATE_OP_CONTEXT(rel_9_delta_pointsTo_op_ctxt,rel_9_delta_pointsTo->createContext());
CREATE_OP_CONTEXT(rel_10_new_pointsTo_op_ctxt,rel_10_new_pointsTo->createContext());
CREATE_OP_CONTEXT(rel_4_alias_op_ctxt,rel_4_alias->createContext());
CREATE_OP_CONTEXT(rel_8_pointsTo_op_ctxt,rel_8_pointsTo->createContext());
pfor(auto it = part.begin(); it<part.end(); ++it) 
try{for(const auto& env0 : *it) {
const Tuple<RamDomain,2> key({{env0[1],0}});
auto range = rel_9_delta_pointsTo->equalRange<0>(key,READ_OP_CONTEXT(rel_9_delta_pointsTo_op_ctxt));
for(const auto& env1 : range) {
if( !rel_8_pointsTo->contains(Tuple<RamDomain,2>({{env0[0],env1[1]}}),READ_OP_CONTEXT(rel_8_pointsTo_op_ctxt))) {
Tuple<RamDomain,2> tuple({{(RamDomain)(env0[0]),(RamDomain)(env1[1])}});
rel_10_new_pointsTo->insert(tuple,READ_OP_CONTEXT(rel_10_new_pointsTo_op_ctxt));
}
}
}
} catch(std::exception &e) { SignalHandler::instance()->error(e.what());}
PARALLEL_END;
}
if(rel_10_new_pointsTo->empty()) break;
rel_8_pointsTo->insertAll(*rel_10_new_pointsTo);
{
auto rel_0 = rel_9_delta_pointsTo;
rel_9_delta_pointsTo = rel_10_new_pointsTo;
rel_10_new_pointsTo = rel_0;
}
rel_10_new_pointsTo->purge();
}
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_9_delta_pointsTo->purge();
if (!isHintsProfilingEnabled() && (performIO || 1)) rel_10_new_pointsTo->purge();
if (performIO) {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","a\to"},{"filename","./output/pointsTo.csv"},{"name","pointsTo"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_pointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_7_new->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_4_alias->purge();
if (!isHintsProfilingEnabled() && (performIO || 0)) rel_8_pointsTo->purge();
}
/* END STRATUM 5 */

// -- relation hint statistics --
if(isHintsProfilingEnabled()) {
std::cout << " -- Operation Hint Statistics --\n";
std::cout << "Relation rel_1_assign:\n";
rel_1_assign->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_2_load:\n";
rel_2_load->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_3_store:\n";
rel_3_store->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_4_alias:\n";
rel_4_alias->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_5_delta_alias:\n";
rel_5_delta_alias->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_6_new_alias:\n";
rel_6_new_alias->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_7_new:\n";
rel_7_new->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_8_pointsTo:\n";
rel_8_pointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_9_delta_pointsTo:\n";
rel_9_delta_pointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
std::cout << "Relation rel_10_new_pointsTo:\n";
rel_10_new_pointsTo->printHintStatistics(std::cout,"  ");
std::cout << "\n";
}
SignalHandler::instance()->reset();
}
public:
void run(size_t stratumIndex = (size_t) -1) override { runFunction<false>(".", ".", stratumIndex); }
public:
void runAll(std::string inputDirectory = ".", std::string outputDirectory = ".", size_t stratumIndex = (size_t) -1) override { runFunction<true>(inputDirectory, outputDirectory); }
public:
void printAll(std::string outputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","a\tb"},{"filename","./output/alias.csv"},{"name","alias"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_alias);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"attributeNames","a\to"},{"filename","./output/pointsTo.csv"},{"name","pointsTo"}});
if (!outputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = outputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_pointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void loadAll(std::string inputDirectory = ".") override {
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/assign.facts"},{"name","assign"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_1_assign);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/load.facts"},{"name","load"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_2_load);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/store.facts"},{"name","store"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->readAll(*rel_3_store);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {std::map<std::string, std::string> directiveMap({{"IO","file"},{"filename","./input/new.facts"},{"name","new"}});
if (!inputDirectory.empty() && directiveMap["IO"] == "file" && directiveMap["filename"].front() != '/') {directiveMap["filename"] = inputDirectory + "/" + directiveMap["filename"];}
IODirectives ioDirectives(directiveMap);
IOSystem::getInstance().getReader(SymbolMask({1, 1}), symTable, ioDirectives, 0)->readAll(*rel_7_new);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpInputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_1_assign");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_1_assign);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_2_load");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_2_load);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_3_store");
IOSystem::getInstance().getWriter(SymbolMask({1, 1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_3_store);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_7_new");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_7_new);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
void dumpOutputs(std::ostream& out = std::cout) override {
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_4_alias");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_4_alias);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
try {IODirectives ioDirectives;
ioDirectives.setIOType("stdout");
ioDirectives.setRelationName("rel_8_pointsTo");
IOSystem::getInstance().getWriter(SymbolMask({1, 1}), symTable, ioDirectives, 0)->writeAll(*rel_8_pointsTo);
} catch (std::exception& e) {std::cerr << e.what();exit(1);}
}
public:
const SymbolTable &getSymbolTable() const override {
return symTable;
}
};
SouffleProgram *newInstance_exmaple(){return new Sf_exmaple;}
SymbolTable *getST_exmaple(SouffleProgram *p){return &reinterpret_cast<Sf_exmaple*>(p)->symTable;}
#ifdef __EMBEDDED_SOUFFLE__
class factory_Sf_exmaple: public souffle::ProgramFactory {
SouffleProgram *newInstance() {
return new Sf_exmaple();
};
public:
factory_Sf_exmaple() : ProgramFactory("exmaple"){}
};
static factory_Sf_exmaple __factory_Sf_exmaple_instance;
}
#else
}
int main(int argc, char** argv)
{
try{
souffle::CmdOptions opt(R"(exmaple.dl)",
R"(.)",
R"(.)",
false,
R"()",
1,
-1);
if (!opt.parse(argc,argv)) return 1;
#if defined(_OPENMP) 
omp_set_nested(true);
#endif
souffle::Sf_exmaple obj;
obj.runAll(opt.getInputFileDir(), opt.getOutputFileDir(), opt.getStratumIndex());
return 0;
} catch(std::exception &e) { souffle::SignalHandler::instance()->error(e.what());}
}
#endif
