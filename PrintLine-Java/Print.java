import java.io.*;

public class Print {
	public static void main(String args[]) throws Exception {
		File file = new File(args[0]);
		File fl = new File(args[1]);
		int total = getTotalLines(fl);
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		String st; 
		st = br.readLine();
		String[] array = st.split(",");
		int[] arr = new int[array.length];
		try {
			for (int i = 0; i < array.length; i++) {
				arr[i] = Integer.parseInt(array[i]);
			}
		} catch (Exception e) {
			
		}
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]-2 > 0 && arr[i] + 2 < total) {
				System.out.print(arr[i]-2 + " ");
				readLine(fl,arr[i]-2);
				System.out.print(arr[i]-1);
				readLine(fl,arr[i]-1);
				System.out.print(arr[i] + " ");
				readLine(fl,arr[i]);
				System.out.print(arr[i]+1 + " ");
				readLine(fl,arr[i]+1);
				System.out.print(arr[i]+2 + " ");
				readLine(fl,arr[i]+2);
				System.out.println("");
			} else
			if (arr[i]-1 > 0 && arr[i] + 1 < total) {
				System.out.print(arr[i]-1 + " ");
				readLine(fl,arr[i]-1);
				System.out.print(arr[i] + " ");
				readLine(fl,arr[i]);
				System.out.print(arr[i]+1 + " ");
				readLine(fl,arr[i]+1);
				System.out.println("");
			} else {
				System.out.print(arr[i] + " ");
				readLine(fl,arr[i]);
			}
        }
	}
	
	static void readLine(File sourceFile, int lineNumber)
            throws IOException {
        FileReader in = new FileReader(sourceFile);
        LineNumberReader reader = new LineNumberReader(in);
        String s = "";
        int lines = 0;
        while (s != null) {
            lines++;
            s = reader.readLine();
            if((lines - lineNumber) == 0) {
             System.out.println(s);
            }
        }
        reader.close();
        in.close();
    }
	
	static int getTotalLines(File file) throws IOException {
        FileReader in = new FileReader(file);
        LineNumberReader reader = new LineNumberReader(in);
        String s = reader.readLine();
        int lines = 0;
        while (s != null) {
            lines++;
            s = reader.readLine();
        }
        reader.close();
        in.close();
        return lines;
    }
	
}
