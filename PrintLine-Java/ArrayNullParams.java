class Rex {
	private int grade;

	public Rex(int grade){
		this.grade = grade;
	}

	public String favouriteSubject(String[] subjects){
		for(int i = 0; i < subjects.length; i++){
			if(subjects[i] == "INFO3333") {
				return subjects[i];
			}
		}
		return null;
	}

	public int getGrade() { return grade; }
}

public class ArrayNullParams {
	public static void main(String[] args){
		Rex rex = new Rex(10);
		String[] rexSubjects = null;
		rex.favouriteSubject(rexSubjects);
	}
}