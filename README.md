# INFO3600-bugchecker - Digger
Digger - a University of Sydney's undergrats capstone project developed and maintained by Goal Diggers

## Team Member: ##
* Gavin Fungtammasan
* Rex Shen
* Lucas Huang
* Yang Lin

## Objective
We are aiming to develop a Java null-pointer exception checker that using [Doop](http://doop.program-analysis.org) to perform static analysis on any Java program.:no_entry_sign::bug:

## Association Partner
We are devloping this product with the aid from [Synopsys](https://www.synopsys.com)

## Digger Tutorial

- Git
	- Dependencies: *Bash 4.0+, pyhton3, [Souffl�](https://souffle-lang.github.io/docs/build/)*
	- ```export DIGGER_HOME=path/to/this/repository```
	- ```export PATH=$PATH:$DIGGER_HOME/digger``` 
	- ```cd path/to/test/folder``` with ```results.json``` file storing expected results.  
		E.g.```{"accessNullArray":[1, 2], "storeNullArray":[10]}```
	- ```diggerPlus -d [doop-analysis-options] -a [must|may]```

- Docker
	- Dependencies: *[Docker](https://www.docker.com/get-started)*
	- ```docker pull jshe9611/digger```
	- ```cd path/to/test/folder``` with ```results.json``` file storing expected results.  
		E.g.```{"accessNullArray":[1, 2], "storeNullArray":[10]}```
	- ```docker run -it -v "$(pwd)":/mnt jshe9611/digger``` 
	- ```diggerPlus -d [doop-analysis-options] -a [must|may]```

    
    
Check [here](https://bitbucket.org/Rex9611/info3600-bugchecker/src/master/Doop-Help.txt) to find out more about doop flag
