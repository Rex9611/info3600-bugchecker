
public class NestedNull {


  public static void main(String[] args) {
    String a = null;
    if (true) {
      a = "";
      while(true) {
        a = null;
        if (true) {
          a.toLowerCase();
        }
        else {
          a = "";
        }
      }
    }

  }
}
