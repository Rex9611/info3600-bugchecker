// Null is passed to a method which invokes a function on it. This
// method is repeated in an endless loop
public class PassNullLoop {

    public void foo(String s) {
      System.out.println(s);
    }

    public static void main(String[] args) {

      while(1) {
        foo(null);
      }

    }
}
