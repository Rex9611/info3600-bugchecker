class Rex {
	private int grade;
	public int age;

	public Rex(int grade){
		this.grade = grade;
	}

	public int getGrade() { return grade; }
}

public class ArrayNullObjectLoad {
	public static void main(String[] args){
		Rex[] array = null;
		int age = array[0].age;
	}
}