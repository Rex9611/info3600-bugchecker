public class NullObjectMiniLoop {

    public static NullObjectMiniLoop createObject() {
      return null;
    }


    public static void main(String[] args) {

      for (int i = 0; i < 5; i++) {
        NullObjectMiniLoop t = createObject();
        String str = t.toString();
      }

    }
}
