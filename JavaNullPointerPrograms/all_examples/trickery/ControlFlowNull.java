
public class ControlFlowNull {

  public static void main(String[] args) {

    String a = null;
    String b = "";
    for (int i = 0; i < 5; i ++) {
      a = b;
      b = "";
      if (i == 2) {
        a = null;
        break;
      }
    }
    a.toLowerCase();
  }
}
