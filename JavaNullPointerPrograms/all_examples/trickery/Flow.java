class A {
	private int id;
	public A(int id) { this.id = id; }
	public int getId() { return id; }
}

public class Flow {
	
	public static void main(String[] args){
		A a = null;
		if(isNull(a)){
			a = new A(1);
		}
		if(a.getId() == 1) {
			a = new A(2);
		}
		int a_id = a.getId();
	}

	public static boolean isNull(A a) {
		return a == null;
	}
}